FROM java:8-alpine

COPY . /app/soenda/sdmx-service
WORKDIR /app/soenda/sdmx-service

COPY env.docker.properties /app/soenda/sdmx-service/env.properties

RUN sh -c "./gradlew build -x test"

EXPOSE 8090
ENTRYPOINT ["sh", "-c", "./gradlew bootRun"]
