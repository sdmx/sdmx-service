package org.soenda.service.v1;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SdmxDataControllerTest {
  @Autowired private MockMvc mockMvc;

  private static String[] sources = new String[] { "api:bps", "sdmx:EUROSTAT", /* "fame:bi" */ };

  private static final Logger log = LoggerFactory.getLogger(SdmxDataControllerTest.class);

  public static Stream<Arguments> whenListDataflow_thenNotEmpty() {
    String[] queries = new String[] {"", "a"};
    Integer[] pages = new Integer[] {1, 2};
    Stream<Arguments> params = combineParametersAsStream(sources, queries, pages);

    return params;
  }

  @ParameterizedTest
  @MethodSource
  public void whenListDataflow_thenNotEmpty(String source, String query, int page)
      throws Exception {
    MockHttpServletRequestBuilder reqBuilder =
        MockMvcRequestBuilders.get("/v1/data/sdmx/{source}", source)
            .param("q", query)
            .param("page", Integer.toString(page))
            .accept(MediaType.APPLICATION_JSON);

    ResultActions resultActions = mockMvc.perform(reqBuilder);
    MockHttpServletResponse mvcResponse = resultActions.andReturn().getResponse();

    log.debug("LOG STATUS: " + mvcResponse.getStatus());

    resultActions
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
  }

  /* Extended */

  protected static List<Arguments> combineParameters(Object[][] params, int i, List<Object> tmpArg) {
    List<Arguments> args = new ArrayList<>();

    for (Object value : params[i]) {
      tmpArg.add(value.toString());

      if ((params.length - 1) > i) {
        args.addAll(combineParameters(params, i + 1, tmpArg));
        tmpArg = tmpArg.subList(0, i);
      }
      else {
        args.add(Arguments.of(tmpArg.toArray()));
        tmpArg.remove(tmpArg.size() - 1);
      }
    }

    return args;
  }

  protected static Stream<Arguments> combineParametersAsStream(Object[] ...params) {
    return Stream.of(combineParameters(params, 0, new ArrayList<>()).toArray(new Arguments[0]));
  }
}
