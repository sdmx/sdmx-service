package org.soenda.service.connector.bps;

import org.soenda.service.connector.bps.object.*;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BpsService {

  public static final String LANG_ID = "ind";
  public static final String LANG_EN = "eng";

  @GET("domain")
  Call<ResponseData> getDomains(
      @Query("key") String key, @Query("type") String type, @Query("prov") String prov);

  @GET("list?model=subject")
  Call<ResponseData> getSubjects(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("subcat") String subcat,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=subcat")
  Call<ResponseData> getSubjectCategories(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=var")
  Call<ResponseData> getVariables(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("subject") String subject,
      @Query("vervar") String vervar,
      @Query("year") String year,
      @Query("area") boolean area,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=turvar")
  Call<ResponseData> getDerivedVariables(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String var,
      @Query("group") String group,
      @Query("nopage") boolean nopage,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=vervar")
  Call<ResponseData> getVerticalVariables(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String var,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=th")
  Call<ResponseData> getPeriodData(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String var,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=turth")
  Call<ResponseData> getDerivedPeriodData(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String var,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=data")
  Call<DataContent> getData(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String var,
      @Query("turvar") String turvar,
      @Query("vervar") String vervar,
      @Query("th") String th,
      @Query("turth") String turth,
      @Query("lang") String lang);

  @GET("list?model=unit")
  Call<ResponseData> getUnitData(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=indicators")
  Call<ResponseData> getIndicators(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("var") String subcat,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("list?model=statictable")
  Call<ResponseData> getStatisticTables(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("year") String year,
      @Query("month") String month,
      @Query("page") Integer page,
      @Query("lang") String lang);

  @GET("view?model=statictable")
  Call<ResponseStatisticTable> getStatisticTable(
      @Query("key") String key,
      @Query("domain") String domain,
      @Query("id") String id,
      @Query("lang") String lang);
}
