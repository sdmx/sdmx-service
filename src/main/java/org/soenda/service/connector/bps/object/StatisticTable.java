package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import lombok.Data;
import org.joda.time.DateTime;
import org.soenda.service.support.serializer.DateTimeDeserializer;
import org.soenda.service.support.serializer.DateTimeSerializer;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticTable implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String tableId;

  @JsonProperty String title;

  @JsonProperty
  @JsonAlias({"subj_id", "sub_id"})
  String subjId;

  @JsonProperty String subj;

  @JsonProperty String table;

  @JsonProperty
  @JsonDeserialize(using = DateTimeDeserializer.class)
  @JsonSerialize(using = DateTimeSerializer.class)
  DateTime crDate;

  @JsonProperty
  @JsonDeserialize(using = DateTimeDeserializer.class)
  @JsonSerialize(using = DateTimeSerializer.class)
  DateTime updtDate;

  @JsonProperty String size;

  @JsonProperty String excel;
}
