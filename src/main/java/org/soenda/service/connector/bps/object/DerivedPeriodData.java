package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DerivedPeriodData implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String turthId;

  @JsonProperty String turth;

  @JsonProperty String groupTurthId;

  @JsonProperty String nameGroupTurth;
}
