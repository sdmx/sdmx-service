package org.soenda.service.connector.bps.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.soenda.service.connector.bps.object.*;

public class DataContentDeserializer extends StdDeserializer<DataContent> {

  private static final long serialVersionUID = 1L;

  public DataContentDeserializer() {
    this(null);
  }

  public DataContentDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public DataContent deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    DataContent result = new DataContent();
    JsonNode node = jp.getCodec().readTree(jp);

    result.setStatus(node.get("status").asText());
    result.setDataAvailability(node.get("data-availability").asText());
    result.setLabelvervar(node.get("labelvervar").asText());
    deserializeVariables(result, node.get("var"));
    deserializeDerivedVariables(result, node.get("turvar"));
    deserializeVerticalVariables(result, node.get("vervar"));
    deserializePeriodData(result, node.get("tahun"));
    deserializeDerivedPeriodData(result, node.get("turtahun"));
    deserializeDataContent(result, (ObjectNode) node.get("datacontent"));

    return result;
  }

  private void deserializeVariables(DataContent result, JsonNode node) {
    List<Variable> list = new ArrayList<>();

    for (JsonNode nodeItem : node) {
      Variable item = new Variable();
      item.setVarId(nodeItem.get("val").asText());
      item.setTitle(nodeItem.get("label").asText());
      item.setUnit(nodeItem.get("unit").asText());
      item.setSubName(nodeItem.get("subj").asText());
      item.setDef(nodeItem.get("def").asText());
      item.setNotes(nodeItem.get("note").asText());
      list.add(item);
    }

    result.setVar(list);
  }

  private void deserializeDerivedVariables(DataContent result, JsonNode node) {
    List<DerivedVariable> list = new ArrayList<>();

    for (JsonNode nodeItem : node) {
      DerivedVariable item = new DerivedVariable();
      item.setTurvarId(nodeItem.get("val").asText());
      item.setTurvar(nodeItem.get("label").asText());
      list.add(item);
    }

    result.setTurvar(list);
  }

  private void deserializeVerticalVariables(DataContent result, JsonNode node) {
    List<VerticalVariable> list = new ArrayList<>();

    for (JsonNode nodeItem : node) {
      VerticalVariable item = new VerticalVariable();
      item.setKodeVerId(nodeItem.get("val").asText());
      item.setVervar(nodeItem.get("label").asText());
      list.add(item);
    }

    result.setVervar(list);
  }

  private void deserializePeriodData(DataContent result, JsonNode node) {
    List<PeriodData> list = new ArrayList<>();

    for (JsonNode nodeItem : node) {
      PeriodData item = new PeriodData();
      item.setThId(nodeItem.get("val").asText());
      item.setTh(nodeItem.get("label").asText());
      list.add(item);
    }

    result.setTahun(list);
  }

  private void deserializeDerivedPeriodData(DataContent result, JsonNode node) {
    List<DerivedPeriodData> list = new ArrayList<>();

    for (JsonNode nodeItem : node) {
      DerivedPeriodData item = new DerivedPeriodData();
      item.setTurthId(nodeItem.get("val").asText());
      item.setTurth(nodeItem.get("label").asText());
      list.add(item);
    }

    result.setTurtahun(list);
  }

  private void deserializeDataContent(DataContent result, ObjectNode node) {
    Map<String, Double> data = new HashMap<>();

    Iterator<Map.Entry<String, JsonNode>> iterator = node.fields();

    while (iterator.hasNext()) {
      Map.Entry<String, JsonNode> entry = iterator.next();
      data.put(entry.getKey(), entry.getValue().asDouble());
    }

    result.setDatacontent(data);
  }
}
