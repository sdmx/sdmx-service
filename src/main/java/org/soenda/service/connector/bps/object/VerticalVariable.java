package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerticalVariable implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String kodeVerId;

  @JsonProperty String vervar;

  @JsonProperty String itemVerId;

  @JsonProperty String groupVerId;

  @JsonProperty String nameGroupVerId;
}
