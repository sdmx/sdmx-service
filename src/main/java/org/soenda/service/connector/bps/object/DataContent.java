package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.Data;
import org.soenda.service.connector.bps.serializer.DataContentDeserializer;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = DataContentDeserializer.class)
public class DataContent implements Serializable {
  // private static final long serialVersionUID = -6330037177433871052L;
  private static final long serialVersionUID = 1L;

  @JsonProperty List<Variable> var;

  @JsonProperty List<DerivedVariable> turvar;

  @JsonProperty String labelvervar;

  @JsonProperty List<VerticalVariable> vervar;

  @JsonProperty List<PeriodData> tahun;

  @JsonProperty List<DerivedPeriodData> turtahun;

  @JsonProperty Map<String, Double> datacontent;

  @JsonProperty String status;

  @JsonProperty String dataAvailability;

  // datacontent:[vervar{4}][var][turvar][tahun][turtahun]
  public Double getValue(
      String vervar, String varId, String turvar, String tahun, String turtahun) {
    String key = String.join("", vervar, varId, turvar, tahun, turtahun);

    if (datacontent.containsKey(key)) {
      return datacontent.get(key);
    } else {
      return 0d;
    }
  }

  public boolean hasVariable() {
    return !var.isEmpty();
  }
}
