package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import org.soenda.service.connector.bps.serializer.ListSerializer;

@Data
@JsonDeserialize(using = ListSerializer.class)
public class ResponseData implements Serializable {
  private static final long serialVersionUID = 1L;

  String status;

  String dataAvailability;

  Pagination pagination;

  List<Domain> domains;

  List<Subject> subjects;

  List<SubjectCategory> subjectCategories;

  List<Indicator> indicators;

  List<StatisticTable> statisticTables;

  List<Variable> variables;

  List<DerivedVariable> derivedVariables;

  List<VerticalVariable> verticalVariables;

  List<PeriodData> periodData;

  List<DerivedPeriodData> derivedPeriodData;

  List<UnitData> unitData;
}
