package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subject implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String subId;

  @JsonProperty String subcatId;

  @JsonProperty String title;

  @JsonProperty String subcat;

  @JsonProperty int ntabel;
}
