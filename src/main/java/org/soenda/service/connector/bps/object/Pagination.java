package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagination implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty Integer page;

  @JsonProperty Integer pages;

  @JsonProperty Integer perPage;

  @JsonProperty Integer count;

  @JsonProperty Integer total;

  public boolean hasPagination() {
    return pages > 1;
  }
}
