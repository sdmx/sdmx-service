package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Variable implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String varId;

  @JsonProperty String title;

  @JsonProperty String subId;

  @JsonProperty String subName;

  @JsonProperty String def;

  @JsonProperty String notes;

  @JsonProperty String vertical;

  @JsonProperty String unit;

  @JsonProperty String graphId;

  @JsonProperty String graphName;

  @JsonProperty String turth;

  @JsonProperty String turvar;

  @JsonProperty String jenis;
}
