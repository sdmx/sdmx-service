package org.soenda.service.connector.bps.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Indicator implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty String var;

  @JsonProperty String indicatorId;

  @JsonProperty String title;

  @JsonProperty String name;

  @JsonProperty String dataSource;

  @JsonProperty Double value;

  @JsonProperty String unit;
}
