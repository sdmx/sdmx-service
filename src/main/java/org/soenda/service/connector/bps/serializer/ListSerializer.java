package org.soenda.service.connector.bps.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.support.serializer.NodeSerializer;

public class ListSerializer extends StdDeserializer<ResponseData> {

  private static final long serialVersionUID = 1L;

  public ListSerializer() {
    this(null);
  }

  public ListSerializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public ResponseData deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    ResponseData response = new ResponseData();
    JsonNode node = jp.getCodec().readTree(jp);
    response.setStatus(node.get("status").asText());
    response.setDataAvailability(node.get("data-availability").asText());

    for (final JsonNode dataNode : node.get("data")) {
      if (dataNode.isArray()) {
        guessDataContent(response, dataNode);
      }
      // pagination
      else {
        Pagination pagination = new Pagination();
        pagination.setPage(dataNode.get("page").asInt());
        pagination.setPages(dataNode.get("pages").asInt());
        pagination.setTotal(dataNode.get("total").asInt());

        if (dataNode.has("per_page")) {
          pagination.setPerPage(dataNode.get("per_page").asInt());
          pagination.setCount(dataNode.get("count").asInt());
        }

        response.setPagination(pagination);
      }
    }

    return response;
  }

  private void guessDataContent(ResponseData response, JsonNode dataNode)
      throws JsonProcessingException {
    JsonNode sample = dataNode.get(0);

    // Domain
    if (sample.has("domain_id")) {
      NodeSerializer<Domain> serializer = new NodeSerializer<>();
      response.setDomains(serializer.deserializeArray(dataNode, Domain.class));
    }
    // Variables
    else if (sample.has("var_id")) {
      NodeSerializer<Variable> serializer = new NodeSerializer<>();
      response.setVariables(serializer.deserializeArray(dataNode, Variable.class));
    }
    // Derived Variables
    else if (sample.has("turvar_id")) {
      NodeSerializer<DerivedVariable> serializer = new NodeSerializer<>();
      response.setDerivedVariables(serializer.deserializeArray(dataNode, DerivedVariable.class));
    }
    // Vertical Variables
    else if (sample.has("kode_ver_id")) {
      NodeSerializer<VerticalVariable> serializer = new NodeSerializer<>();
      response.setVerticalVariables(serializer.deserializeArray(dataNode, VerticalVariable.class));
    }
    // Period Data
    else if (sample.has("th_id")) {
      NodeSerializer<PeriodData> serializer = new NodeSerializer<>();
      response.setPeriodData(serializer.deserializeArray(dataNode, PeriodData.class));
    }
    // Derived Period Data
    else if (sample.has("turth_id")) {
      NodeSerializer<DerivedPeriodData> serializer = new NodeSerializer<>();
      response.setDerivedPeriodData(serializer.deserializeArray(dataNode, DerivedPeriodData.class));
    }
    // Unit Data
    else if (sample.has("unit_id")) {
      NodeSerializer<UnitData> serializer = new NodeSerializer<>();
      response.setUnitData(serializer.deserializeArray(dataNode, UnitData.class));
    }
    // Indicators
    else if (sample.has("indicator_id")) {
      NodeSerializer<Indicator> serializer = new NodeSerializer<>();
      response.setIndicators(serializer.deserializeArray(dataNode, Indicator.class));
    }
    // Static Tables
    else if (sample.has("table_id")) {
      NodeSerializer<StatisticTable> serializer = new NodeSerializer<>();
      response.setStatisticTables(serializer.deserializeArray(dataNode, StatisticTable.class));
    }
    // Subject
    else if (sample.has("sub_id")) {
      NodeSerializer<Subject> serializer = new NodeSerializer<>();
      response.setSubjects(serializer.deserializeArray(dataNode, Subject.class));
    }
    // Subject Categori
    else if (sample.has("subcat_id")) {
      NodeSerializer<SubjectCategory> serializer = new NodeSerializer<>();
      response.setSubjectCategories(serializer.deserializeArray(dataNode, SubjectCategory.class));
    }
  }
}
