package org.soenda.service.connector.bps.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.soenda.service.connector.bps.object.*;

public class StatisticTableDeserializer extends StdDeserializer<StatisticTable> {

  private static final long serialVersionUID = 1L;

  public StatisticTableDeserializer() {
    this(null);
  }

  public StatisticTableDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public StatisticTable deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = jp.getCodec().readTree(jp);
    JsonNode contentNode = node.get("data");
    StatisticTable statisticTable = new StatisticTable();
    // NodeSerializer<StatisticTable> serializer = new NodeSerializer<>();

    // statisticTable.setTableId();
    // statisticTable.setTitle();
    // statisticTable.setSubjId();
    // statisticTable.setSubj();
    // statisticTable.setTable();
    // statisticTable.setSize();
    // statisticTable.setExcel();
    // statisticTable.setCrDate();
    // statisticTable.setUpdtDate();

    return statisticTable;
    // return serializer.deserialize(contentNode, StatisticTable.class);
    // return new StatisticTable();
  }
}
