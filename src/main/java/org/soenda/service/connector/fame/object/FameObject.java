package org.soenda.service.connector.fame.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FameObject {
  @JsonProperty String ObjectName;
  @JsonProperty String ObjectType;
  @JsonProperty String Frequency;
  @JsonProperty String StartDate;
  @JsonProperty String EndDate;
  @JsonProperty String DataType;
  @JsonProperty String Observed;
  @JsonProperty String FAMEpartition;
  @JsonProperty String KeyFamilyId;
  @JsonProperty String Documentation;
  @JsonProperty String Description;
  @JsonProperty String CollectionIndicator;
  @JsonProperty String MultiplierUnit;
  @JsonProperty String Decimaldiffusion;
  @JsonProperty String Lag;
  @JsonProperty String privateationCalendar;
  @JsonProperty String Coverage;
  @JsonProperty String QualityStandard;
  @JsonProperty String NoteMethodology;

  @JsonProperty String Basis;
  @JsonProperty String Calendar;

  @JsonProperty String Currency;
  @JsonProperty String Source;
  @JsonProperty String Unit;
  @JsonProperty String ArabicDescription;
  @JsonProperty String FormulaExpression;
  @JsonProperty String SdmxName;
}
