package org.soenda.service.connector.fame;

import java.util.List;
import org.soenda.service.connector.fame.object.CustomTiq;
import org.soenda.service.connector.fame.object.FameObject;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface FameService {

  // DB: 'INDO_BOP',
  // SS: 'tot',
  // ENV: 'Dev',
  // SDB: 'INDO_BOP_SCRN',
  // SON: 'DESC.EN',
  @FormUrlEncoded
  @POST("GetFameObjectsService/GetFameObjectByContextWithMetaData")
  Call<List<FameObject>> getFameObjectByContextWithMetaData(
      @Field("ENV") String env,
      @Field("DB") String db,
      @Field("SDB") String sdb,
      @Field("SON") String son,
      @Field("SS") String search);

  @FormUrlEncoded
  @POST("GetFameObjectsService/GetFameObjectWithMetaData")
  Call<List<FameObject>> GetFameObjectWithMetaData(
      @Field("ENV") String env, @Field("DB") String db, @Field("OC") String search);

  // DB: 'INDO_BOP',
  // ENV: 'Dev',
  // OBS: 'EXIM_BI_1_1_M.M.010700.00.01.02.USD.OBS_VALUE',
  // OBSERVE: 'END',
  // START: '04/01/2011',
  // END: '12/31/2011',
  @FormUrlEncoded
  @POST("GetFameObjectsService/GetTiqObjects")
  Call<List<CustomTiq>> getTiqObjects(
      @Field("ENV") String env,
      @Field("DB") String db,
      @Field("OBS") String objects,
      @Field("FRQ") String frequency,
      @Field("OBSERVE") String observe,
      @Field("START") String start,
      @Field("END") String end);
}
