package org.soenda.service.connector.fame.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Observation {
  @JsonProperty String DateIndex; // Index of an object in fame date format
  @JsonProperty Object ObservationValue; // Value of the object
  @JsonProperty Object OldValue; // Old value of the objects (not being used)
}
