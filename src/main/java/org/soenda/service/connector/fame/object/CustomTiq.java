package org.soenda.service.connector.fame.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomTiq {
  @JsonProperty String Name; // Object Name
  @JsonProperty String Type; // Formula/Series/Scalar
  @JsonProperty String Description; // Object Description
  @JsonProperty String Documentation; // Object Documentation
  @JsonProperty List<Observation> Observations; // Data Point Collection
}
