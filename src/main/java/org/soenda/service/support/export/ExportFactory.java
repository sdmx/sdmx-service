package org.soenda.service.support.export;

import org.soenda.service.support.export.product.CSVProduct;
import org.soenda.service.support.export.product.ExcelProduct;
import org.soenda.service.support.export.product.RDFProduct;
import org.soenda.service.support.export.product.XMLProduct;

/** Created by Ramdhan on 09/01/2018. */
public class ExportFactory {

  public static final FileExporter create(String type, String filepath) throws Exception {
    switch (type) {
      case "csv":
        return new CSVProduct(filepath);
      case "rdf":
        return new RDFProduct(filepath);
      case "xml":
        return new XMLProduct(filepath);
      case "xls":
      case "xlsx":
        return new ExcelProduct(filepath, type);
    }

    throw new Exception("Cannot find FileExporter: " + type);
  }
}
