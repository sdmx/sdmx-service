package org.soenda.service.support.wsclient.wsconfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WsConfigObject {
  @JsonProperty String id;
  @JsonProperty String name;
  @JsonProperty EndpointObject endpoint;
}
