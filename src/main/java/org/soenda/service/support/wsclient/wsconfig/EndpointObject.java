package org.soenda.service.support.wsclient.wsconfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EndpointObject {
  @JsonProperty String type;
  @JsonProperty String wsType;
  @JsonProperty String version;
  @JsonProperty String url;
}
