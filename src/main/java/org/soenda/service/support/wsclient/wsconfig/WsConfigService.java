package org.soenda.service.support.wsclient.wsconfig;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WsConfigService {
  @GET("ws-config")
  Call<List<WsConfigObject>> getList();

  @GET("ws-config/{id}")
  Call<WsConfigObject> getConfig(@Path(value = "id", encoded = true) String id);
}
