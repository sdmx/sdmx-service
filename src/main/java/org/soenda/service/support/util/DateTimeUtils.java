package org.soenda.service.support.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.joda.time.DateTime;

public final class DateTimeUtils {

  public static final boolean isValidDate(String value) {
    try {
      DateTime.parse(value);
    } catch (Exception e) {
      return false;
    }

    return true;
  }

  public static final boolean isValidDate(String value, String pattern) {
    SimpleDateFormat formatter = new SimpleDateFormat(pattern);
    formatter.setLenient(false);

    try {
      formatter.parse(value);
      return true;
    } catch (ParseException ex) {
      return false;
    }
  }
}
