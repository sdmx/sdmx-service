package org.soenda.service.support.util;

import java.util.Map;

public final class URLUtils {

  public static final String requestParamBuild(Map<String, Object> param) {
    StringBuilder builder = new StringBuilder();

    for (Map.Entry<String, Object> item : param.entrySet()) {
      if (builder.length() > 0) {
        builder.append("&");
      }

      builder.append(item.getKey() + "=" + item.getValue());
    }

    return builder.toString();
  }

  public static final String getFileUrl(String filepath) {
    return "/file?p=" + filepath.replace(IOUtils.STORAGE_LOCATION + "/", "");
  }

  public static final String getRawFileUrl(String filepath) {
    return "/raw?p=" + filepath.replace(IOUtils.STORAGE_LOCATION + "/", "");
  }
}
