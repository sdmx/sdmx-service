package org.soenda.service.support.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

public final class CollectionUtils {

  public static final String toJson(Object data, PropertyNamingStrategy namingStrategy) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();

      if (namingStrategy != null) {
        objectMapper.setPropertyNamingStrategy(namingStrategy);
      }

      return objectMapper.writeValueAsString(data);
    } catch (Exception e) {
      e.printStackTrace();
      return "{}";
    }
  }

  public static final String toJson(Object data) {
    return toJson(data, null);
  }
}
