package org.soenda.service.support.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.joda.time.DateTime;

public final class StringUtils {

  public static final String uuid() {
    return UUID.randomUUID().toString();
  }

  public static final String timeId() {
    return new SimpleDateFormat("yyMMddHHmmss").format(new Date());
  }

  public static final boolean isValidDate(String date) {
    try {
      DateTime.parse(date);
    } catch (Exception e) {
      return false;
    }

    return true;
  }
}
