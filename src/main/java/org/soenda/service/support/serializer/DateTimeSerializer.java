package org.soenda.service.support.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeSerializer extends JsonSerializer<DateTime> {

  @Override
  public void serialize(
      final DateTime value, final JsonGenerator generator, final SerializerProvider provider)
      throws IOException, JsonProcessingException {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    generator.writeString(value.toString(formatter));
  }
}
