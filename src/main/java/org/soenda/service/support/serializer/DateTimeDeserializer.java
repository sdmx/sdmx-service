package org.soenda.service.support.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import java.io.IOException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeDeserializer extends JsonDeserializer<DateTime>
    implements ContextualDeserializer {

  private JavaType type;

  public DateTimeDeserializer() {}

  public DateTimeDeserializer(JavaType type) {
    this.type = type;
  }

  @Override
  public JsonDeserializer<?> createContextual(DeserializationContext ctx, BeanProperty prop)
      throws JsonMappingException {
    // prop is null when the type to deserialize is the top-level type or a
    // generic type, not a type of a bean property
    JavaType type =
        ctx.getContextualType() != null ? ctx.getContextualType() : prop.getMember().getType();

    return new DateTimeDeserializer(type);
  }

  @Override
  public DateTime deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
    String value = parser.getValueAsString();
    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    DateTime time = DateTime.parse(value.substring(0, 19), formatter);

    return time;
  }
}
