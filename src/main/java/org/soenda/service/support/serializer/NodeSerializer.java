package org.soenda.service.support.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import java.util.ArrayList;
import java.util.List;

public class NodeSerializer<T> {

  private ObjectMapper objectMapper;

  public NodeSerializer() {
    objectMapper = new ObjectMapper();
    objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public List<T> deserializeArray(JsonNode node, Class<T> objClass) throws JsonProcessingException {
    List<T> list = new ArrayList<>();
    objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    for (final JsonNode item : node) {
      list.add(objectMapper.treeToValue(item, objClass));
    }

    return list;
  }

  public T deserialize(JsonNode node, Class<T> objClass) throws JsonProcessingException {
    return objectMapper.treeToValue(node, objClass);
  }
}
