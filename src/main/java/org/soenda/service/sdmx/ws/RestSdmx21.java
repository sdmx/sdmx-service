package org.soenda.service.sdmx.ws;

import java.util.Arrays;
import org.soenda.service.sdmx.parser.DataStructureParser;
import org.soenda.service.sdmx.parser.DataflowParser;
import org.soenda.service.sdmx.parser.DatasetParser;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class RestSdmx21 extends AbstractWsHelper {

  public RestSdmx21(String wsEndpoint) {
    super(wsEndpoint);
  }

  @Override
  public DataflowParser getDataflow(String resourceId, String version) {
    String resource = "dataflow";
    ResponseEntity<String> res = request(getUrl(resource, DEFAULT_AGENCY_ID, resourceId, version));

    return (DataflowParser) getParser(resource, res);
  }

  @Override
  public DataStructureParser getDataStructure(String resourceId) {
    String resource = "datastructure";
    ResponseEntity<String> res = request(getUrl(resource, DEFAULT_AGENCY_ID, "DSD_" + resourceId));

    return (DataStructureParser) getParser(resource, res);
  }

  @Override
  public DatasetParser getDataset(
      String resourceId, String key, String startPeriod, String endPeriod) {
    String resource = "data";
    String url = getUrl("data", resourceId, key);

    if (startPeriod != null) {
      url += "?startPeriod=" + startPeriod;
    }

    if (endPeriod != null) {
      url += startPeriod != null ? "&" : "?";
      url += "endPeriod=" + endPeriod;
    }

    // #dev
    /*if (key.equals("all") && resourceId.equals("migr_pop6ctb")) {
        try {
            InputSource inputSource = new InputSource(new FileInputStream(
                new File(IOUtils.storagePath("ws/data-migr_pop6ctb.xml"))
            ));

            return (DatasetParser) getParser(resource, inputSource);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    return (DatasetParser) getParser(resource, request(url));
  }

  public ResponseEntity<String> request(String url) {
    String reqPath = url.replace(wsEndpoint, "");
    String[] urlParams = reqPath.split("/", 0);

    // Load data from cache
    if (SdmxCache.has(reqPath)) {
      try {
        return ResponseEntity.ok(SdmxCache.get(reqPath));
      } catch (Exception e) {
        System.out.println("Failed to load from cache. [" + reqPath + "]");
      }
    }

    // make a request to the web service
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
    HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

    try {
      ResponseEntity<String> res = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

      if (urlParams.length > 0
      //          && (urlParams[0].equals("dataflow") || urlParams[0].equals("datastructure"))
      ) {
        SdmxCache.put(reqPath, res.getBody());
      }

      return res;
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.ok("Not Found");
    }
  }
}
