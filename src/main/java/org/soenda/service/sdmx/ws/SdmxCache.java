package org.soenda.service.sdmx.ws;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.*;
import java.nio.charset.StandardCharsets;
import org.soenda.service.support.util.IOUtils;

public class SdmxCache {

  public static final String STORAGE_LOCATION = "cache";

  public static boolean has(String resource) {
    return getFile(resource).exists();
  }

  public static File put(String resource, String content) {
    InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
    return put(resource, stream);
  }

  public static File put(String resource, InputStream content) {
    File cacheFile = getFile(resource);
    File dirFile = cacheFile.getParentFile();

    if (!dirFile.exists()) {
      dirFile.mkdirs();
    }

    // prepare tmp dir for data export purposes
    File tmpDir = new File(IOUtils.storagePath("tmp"));

    if (!tmpDir.exists()) {
      tmpDir.mkdirs();
    }

    return IOUtils.filePut(cacheFile.getPath(), content);
  }

  public static File getFile(String resource) {
    String dir = IOUtils.storagePath(STORAGE_LOCATION);
    String filename = resource.replaceAll("[\\?\\&]", "_") + ".xml";

    return new File(dir + "/" + filename);
  }

  public static String get(String resource) throws IOException {
    return Files.toString(SdmxCache.getFile(resource), Charsets.UTF_8);
  }
}
