package org.soenda.service.sdmx.ws;

import org.soenda.service.sdmx.parser.DataStructureParser;
import org.soenda.service.sdmx.parser.DataflowParser;
import org.soenda.service.sdmx.parser.DatasetParser;

public interface SdmxWsHelper {

  public DataflowParser getDataflow(String resourceId, String version);

  public DataStructureParser getDataStructure(String resourceId);

  public DatasetParser getDataset(
      String resourceId, String key, String startPeriod, String endPeriod);
}
