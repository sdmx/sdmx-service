package org.soenda.service.sdmx.ws;

import java.io.StringReader;
import org.soenda.service.sdmx.parser.DataStructureParser;
import org.soenda.service.sdmx.parser.DataflowParser;
import org.soenda.service.sdmx.parser.DatasetParser;
import org.soenda.service.sdmx.parser.SdmxParser;
import org.springframework.http.*;
import org.xml.sax.InputSource;

public abstract class AbstractWsHelper implements SdmxWsHelper {

  protected final String DEFAULT_AGENCY_ID = "ESTAT";

  protected String wsEndpoint;

  public AbstractWsHelper(String wsEndpoint) {
    this.wsEndpoint = wsEndpoint;
  }

  @Override
  public abstract DataflowParser getDataflow(String resourceId, String version);

  @Override
  public abstract DataStructureParser getDataStructure(String resourceId);

  @Override
  public abstract DatasetParser getDataset(
      String resourceId, String key, String startPeriod, String endPeriod);

  public DataflowParser getDataflow() {
    return getDataflow("all", "latest");
  }

  public DatasetParser getDataset(String resourceId, String key) {
    return getDataset(resourceId, key, null, null);
  }

  protected String getUrl(String resource, String... params) {
    String url = wsEndpoint + resource;

    for (String param : params) {
      url += "/" + param;
    }

    return url;
  }

  protected SdmxParser getParser(String resource, ResponseEntity<String> res) {
    return getParser(resource, new InputSource(new StringReader(res.getBody())));
  }

  protected SdmxParser getParser(String resource, InputSource res) {
    try {
      switch (resource) {
        case "dataflow":
          return new DataflowParser(res);
        case "data":
          return new DatasetParser(res);
        case "datastructure":
          return new DataStructureParser(res);
        default:
          throw new Exception("There is no parser for '" + resource + "' resource.");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }
}
