package org.soenda.service.sdmx.datasource;

import java.io.File;
import org.soenda.service.sdmx.parser.DataStructureParser;
import org.soenda.service.sdmx.parser.DataflowParser;
import org.soenda.service.sdmx.parser.DatasetParser;
import org.soenda.service.sdmx.ws.RestSdmx21;
import org.soenda.service.sdmx.ws.SdmxWsHelper;
import org.soenda.service.support.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SdmxData implements SdmxWsHelper {

  private String wsMethod = "rest";

  private String wsVersion = "2.1";

  @Autowired private Environment env;

  public String getWsMethod() {
    return wsMethod;
  }

  public void setWsMethod(String wsMethod) {
    this.wsMethod = wsMethod;
  }

  public String getWsVersion() {
    return wsVersion;
  }

  public void setWsVersion(String wsVersion) {
    this.wsVersion = wsVersion;
  }

  public SdmxWsHelper getWsHelper() {
    return getWsHelper(wsMethod, wsVersion);
  }

  public SdmxWsHelper getWsHelper(String wsMethod, String wsVersion) {
    String wsEndpoint = "sdmx." + wsMethod + ".v" + wsVersion.replace(".", "") + ".";

    switch (wsMethod) {
      case "soap":
        // TODO: SOAP WS Helper
        if (wsVersion == "2.1") {
          return new RestSdmx21(env.getProperty(wsEndpoint + "wsdl"));
        } else return new RestSdmx21(env.getProperty(wsEndpoint + "wsdl"));

      case "rest":
        return new RestSdmx21(env.getProperty(wsEndpoint + "url"));
    }

    return null;
  }

  @Override
  public DataflowParser getDataflow(String resourceId, String version) {
    return getWsHelper().getDataflow(resourceId, version);
  }

  @Override
  public DataStructureParser getDataStructure(String resourceId) {
    return getWsHelper().getDataStructure(resourceId);
  }

  @Override
  public DatasetParser getDataset(
      String resourceId, String key, String startPeriod, String endPeriod) {
    return getWsHelper().getDataset(resourceId, key, startPeriod, endPeriod);
  }

  public File getMetadata(String resourceId) {
    String dir = IOUtils.storagePath("metadata") + "/";
    String filepath = dir + resourceId + ".html";

    File file = new File(filepath);

    return file.exists() ? file : new File(dir + "default.html");
  }
}
