package org.soenda.service.sdmx.datasource.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UnknownDataSourceException extends Exception {

  private static final long serialVersionUID = 1L;

  private String source;

  public UnknownDataSourceException(String source) {
    this.source = source;
  }

  @Override
  public String getMessage() {
    return String.format("Data source '%s' is unknown.", source);
  }

  public ResponseEntity<String> getResponseEntity() {
    ObjectMapper mapper = new ObjectMapper();
    ObjectNode response = mapper.createObjectNode();
    response.putPOJO("message", getMessage());

    return new ResponseEntity<>(response.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
