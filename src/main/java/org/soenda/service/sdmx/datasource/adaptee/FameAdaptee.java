package org.soenda.service.sdmx.datasource.adaptee;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import lombok.Getter;
import lombok.Setter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.soenda.service.connector.fame.FameService;
import org.soenda.service.connector.fame.object.CustomTiq;
import org.soenda.service.connector.fame.object.FameObject;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.soenda.service.sdmx.parser.FameDataStructureParser;
import org.soenda.service.sdmx.parser.FameParser;
import org.soenda.service.sdmx.ws.SdmxCache;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.xml.sax.InputSource;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class FameAdaptee implements Adaptee {

  public static final String FREQ_MONTHLY = "MONTHLY";
  public static final String FREQ_QUARTERLY = "QUARTERLY";
  public static final String FREQ_ANNUAL = "ANNUAL";
  public static final String OBSERVE_END = "END";

  @Getter @Setter private String endpoint;

  @Getter @Setter private String dsdEndpoint;

  private FameService service;

  private Map<String, String> params;

  public FameAdaptee(String endpoint, Map<String, String> params) {
    this.endpoint = endpoint;
    this.params = params;
    dsdEndpoint = endpoint;

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient client =
        new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)
            // .addInterceptor(interceptor)
            .build();

    Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(JacksonConverterFactory.create())
            .client(client)
            .build();

    service = retrofit.create(FameService.class);
  }

  public List<Dataflow> getDataflow(String search) {
    try {
      Call<List<FameObject>> call =
          service.GetFameObjectWithMetaData(params.get("ENV"), params.get("DB"), search);

      return new FameParser().parseDataflow(call.execute().body());
    } catch (Exception e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  @Override
  public Dataflow getDataflowById(String id) {
    return new Dataflow();
  }

  @Override
  public Page<Dataflow> getDataflow(String search, Integer pageSize, Integer page) {
    List<Dataflow> dataflow = getDataflow(search);

    // make pagination
    PageRequest pageRequest = PageRequest.of(page, pageSize);
    int total = dataflow.size();
    int start = (int) pageRequest.getOffset();
    int end = (start + pageSize) > total ? total : start + pageSize;
    List<Dataflow> data = start < end ? dataflow.subList(start, end) : new ArrayList<>();

    return new PageImpl<>(data, pageRequest, total);
  }

  @Override
  public List<DataSeries> getDataset(String id, Map<String, Object> filter) {
    try {
      Call<List<CustomTiq>> call =
          service.getTiqObjects(
              params.get("ENV"),
              params.get("DB"),
              filter.getOrDefault("obs", "*.OBS_VALUE").toString(),
              filter.getOrDefault("frequency", FREQ_ANNUAL).toString(),
              filter.getOrDefault("observe", OBSERVE_END).toString(),
              filter.getOrDefault("startPeriod", "").toString(),
              filter.getOrDefault("endPeriod", "").toString());

      return new FameParser().parseDataset(call.execute().body(), getDsd(id));
    } catch (Exception e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  @Override
  public DataStructure getDsd(String id) {
    try {
      File dsdFile = getDsdFile(id);

      if (dsdFile == null) {
        return null;
      }

      return new FameDataStructureParser(new InputSource(new FileInputStream(dsdFile))).get();
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println(String.format("DSD '%s' File Not Found", id));

      return null;
    }
  }

  public File getDsdFile(String id) {
    try {
      String dsdCode = id.split("\\.")[0];
      File dsdFile = null;

      // try to load from cache if exists
      if (SdmxCache.has(dsdCode)) {
        try {
          return SdmxCache.getFile(dsdCode);
        } catch (Exception e) {
          System.out.println("Failed to load from cache. [" + dsdCode + "]");
        }
      }

      if (dsdFile == null) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String url = dsdEndpoint + "sdmx.aspx?dsd=" + dsdCode;
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();

        dsdFile = SdmxCache.put(dsdCode, response.body().byteStream());
      }

      return dsdFile;
    } catch (Exception e) {
      System.out.println(String.format("DSD '%s' Not Found", id));
      e.printStackTrace();
      return null;
    }
  }
}
