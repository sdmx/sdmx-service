package org.soenda.service.sdmx.datasource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ehcache.CacheManager;
import org.soenda.service.sdmx.datasource.adaptee.Adaptee;
import org.soenda.service.sdmx.datasource.adaptee.BpsAdaptee;
import org.soenda.service.sdmx.datasource.adaptee.FameAdaptee;
import org.soenda.service.sdmx.datasource.adaptee.SdmxAdaptee;
import org.soenda.service.sdmx.datasource.exception.UnknownDataSourceException;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class DataSourceAdapter {

  @Autowired private Environment env;

  @Autowired private CacheManager cacheManager;

  public Page<Dataflow> getDataflow(String source, String keyword, Pageable pageable)
      throws Exception {
    return getDataflow(source, keyword, pageable.getPageSize(), pageable.getPageNumber());
  }

  public Page<Dataflow> getDataflow(String source, String keyword, Integer pageSize, Integer page)
      throws Exception {
    return getAdaptee(source).getDataflow(keyword, pageSize, page);
  }

  public Dataflow getDataflowById(String source, String id) throws Exception {
    return getAdaptee(source).getDataflowById(id);
  }

  public List<DataSeries> getDataset(String source, String id, Map<String, Object> params)
      throws Exception {
    return getAdaptee(source).getDataset(id, params);
  }

  public DataStructure getDsd(String source, String id) throws Exception {
    return getAdaptee(source).getDsd(id);
  }

  public Adaptee getAdaptee(String source) throws Exception {
    String[] provider = source.split(":");

    if (provider.length < 2) {
      throw new Exception("Invalid data source format. It should be 'source:provider'.");
    }

    switch (provider[0]) {
      case "fame":
        String endpoint = env.getProperty("fame.rest.endpoint");

        Map<String, String> params = new HashMap<>();
        params.put("ENV", env.getProperty("fame.rest.env"));
        params.put("DB", env.getProperty("fame.rest.db"));
        params.put("SDB", env.getProperty("fame.rest.sdb"));
        params.put("SON", env.getProperty("fame.rest.son"));

        return new FameAdaptee(endpoint, params);

      case "sdmx":
        try {
          return new SdmxAdaptee(cacheManager, provider[1]);
          // SdmxProvider provider = new SdmxProvider(source);
          // return new RSdmxAdaptee(provider);
        } catch (Exception e) {
          e.printStackTrace();
          throw new UnknownDataSourceException(source);
        }

      case "api":
        switch (provider[1]) {
          case "bps":
            return new BpsAdaptee(cacheManager);

          default:
            throw new UnknownDataSourceException(source);
        }

      default:
        throw new UnknownDataSourceException(provider[0]);
    }
  }
}
