package org.soenda.service.sdmx.datasource.adaptee;

import it.bancaditalia.oss.sdmx.api.Codelist;
import it.bancaditalia.oss.sdmx.api.PortableDataSet;
import it.bancaditalia.oss.sdmx.client.SdmxClientHandler;
import java.util.*;
import org.ehcache.*;
import org.ehcache.config.builders.*;
import org.ehcache.config.units.*;
import org.soenda.service.sdmx.object.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class SdmxAdaptee implements Adaptee {

  private String provider;
  private CacheManager cacheManager;
  private ResourcePoolsBuilder rPoolsBuilder;

  public SdmxAdaptee(CacheManager cacheManager, String provider) {
    this.provider = provider;
    this.cacheManager = cacheManager;
    this.rPoolsBuilder =
        ResourcePoolsBuilder.newResourcePoolsBuilder()
            // .heap(10, EntryUnit.ENTRIES)
            // .offheap(1, MemoryUnit.MB)
            .disk(20, MemoryUnit.MB, true);
  }

  public List<Dataflow> getDataflow(String search) {
    // preparing cache pool
    Cache<String, HashMap> cache =
        cacheManager.getCache("sdmx-dataflow", String.class, HashMap.class);

    if (cache == null) {
      cache =
          cacheManager.createCache(
              "sdmx-dataflow",
              CacheConfigurationBuilder.newCacheConfigurationBuilder(
                  String.class, HashMap.class, rPoolsBuilder));
    }

    try {
      HashMap<String, String> flows;
      // cache.remove(provider);

      // get dataflows from cache if exists
      if (cache.containsKey(provider)) {
        flows = (HashMap<String, String>) cache.get(provider);
      } else {
        flows = (HashMap<String, String>) SdmxClientHandler.getFlows(provider, "");
        cache.put(provider, flows);
      }

      // transform into list of dataflow
      ArrayList<Dataflow> results = new ArrayList<>();

      for (Map.Entry<String, String> item : flows.entrySet()) {
        String[] keys = item.getKey().split(",");

        // apply search query
        if (!item.getKey().toLowerCase().contains(search.toLowerCase())
            && !item.getValue().toLowerCase().contains(search.toLowerCase())) {
          continue;
        }

        if (keys.length == 3) {
          results.add(new Dataflow(keys[1], item.getValue(), keys[2]));
        } else {
          results.add(new Dataflow(keys[0], item.getValue()));
        }
      }

      return results;
    } catch (Exception e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  @Override
  public Dataflow getDataflowById(String id) {
    return new Dataflow();
  }

  @Override
  public Page<Dataflow> getDataflow(String search, Integer pageSize, Integer page) {
    List<Dataflow> dataflow = getDataflow(search);

    // make pagination
    PageRequest pageRequest = PageRequest.of(page, pageSize);
    int total = dataflow.size();
    int start = (int) pageRequest.getOffset();
    int end = (start + pageSize) > total ? total : start + pageSize;
    List<Dataflow> data = start < end ? dataflow.subList(start, end) : new ArrayList<>();

    return new PageImpl<>(data, pageRequest, total);
  }

  @Override
  public List<DataSeries> getDataset(String id, Map<String, Object> filter) {
    // sdmx key series
    String key = id + "." + filter.getOrDefault("key", "").toString();
    String startPeriod = filter.getOrDefault("startPeriod", "").toString();
    String endPeriod = filter.getOrDefault("endPeriod", "").toString();
    String cacheKey = String.format("%s/%s/%s-%s", provider, key, startPeriod, endPeriod);

    // preparing cache pool
    Cache<String, ArrayList> cache =
        cacheManager.getCache("sdmx-dataset", String.class, ArrayList.class);

    if (cache == null) {
      cache =
          cacheManager.createCache(
              "sdmx-dataset",
              CacheConfigurationBuilder.newCacheConfigurationBuilder(
                  String.class, ArrayList.class, rPoolsBuilder));
    }

    // get dataset from cache if the cacheKey is exists
    if (cache.containsKey(cacheKey)) {
      return (List<DataSeries>) cache.get(cacheKey);
    }

    try {
      // fetch dataset from webservice
      PortableDataSet<Double> dataset =
          SdmxClientHandler.getTimeSeriesTable(provider, key, startPeriod, endPeriod);

      // transform portable dataset into list of dataseries
      ArrayList<DataSeries> results = new ArrayList<>();
      int colCount = dataset.getColumnCount() - 1;
      int rowCount = dataset.getRowCount() - 1;
      Object[] observations = dataset.getObservations();
      String[] timestamps = dataset.getTimeStamps();

      for (int i = 0; i < rowCount; i++) {
        DataSeries series = new DataSeries();
        series.addObservation(
            new Observation(timestamps[i], Double.parseDouble(observations[i].toString())));

        for (int j = 0; j < colCount; j++) {
          Object value = dataset.getValueAt(i, j);
          series.addKey(dataset.getColumnName(j), value == null ? "NULL" : value.toString());
        }

        results.add(series);
      }

      // cache dataseries
      cache.put(cacheKey, results);

      return results;
    } catch (Exception e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  @Override
  public DataStructure getDsd(String id) {
    String cacheKey = String.format("%s/%s", provider, id);

    // preparing cache pool
    Cache<String, DataStructure> cache =
        cacheManager.getCache("sdmx-dsd", String.class, DataStructure.class);

    if (cache == null) {
      cache =
          cacheManager.createCache(
              "sdmx-dsd",
              CacheConfigurationBuilder.newCacheConfigurationBuilder(
                  String.class, DataStructure.class, rPoolsBuilder));
    }

    // get dataset from cache if the cacheKey is exists
    if (cache.containsKey(cacheKey)) {
      return cache.get(cacheKey);
    }

    try {
      DataStructure dsd = new DataStructure(id);

      // get dsd from webservice
      for (it.bancaditalia.oss.sdmx.api.Dimension dim :
          SdmxClientHandler.getDimensions(provider, id)) {
        Dimension dimension =
            new Dimension(dim.getId(), Integer.toString(dim.getPosition()), false);
        dsd.addDimension(dim.getId(), dimension);

        // Concept
        dimension.setConcept(new Concept(dim.getId(), dim.getName()));

        // Code List
        Codelist dimCodeList = dim.getCodeList();
        CodeList codelist = new CodeList(dimCodeList.getId());
        codelist.setName(dimCodeList.getFullIdentifier());
        codelist.setVersion(dimCodeList.getVersion());
        dimension.setCodeList(codelist);

        for (Map.Entry<String, String> item : dimCodeList.entrySet()) {
          codelist.addCode(item.getKey(), item.getValue());
        }
      }

      // put dsd on cache
      cache.put(cacheKey, dsd);

      return dsd;
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println(String.format("DSD '%s' File Not Found", id));
      return null;
    }
  }
}
