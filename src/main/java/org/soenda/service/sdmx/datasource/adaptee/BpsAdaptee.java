package org.soenda.service.sdmx.datasource.adaptee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.ehcache.*;
import org.ehcache.config.builders.*;
import org.ehcache.config.units.*;
import org.joda.time.DateTime;
import org.soenda.service.connector.bps.BpsService;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.sdmx.object.*;
import org.soenda.service.sdmx.parser.bps.*;
import org.soenda.service.sdmx.parser.bps.object.DataId;
import org.soenda.service.sdmx.parser.bps.object.DataflowQuery;
import org.soenda.service.support.util.DateTimeUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class BpsAdaptee implements Adaptee {

  private CacheManager cacheManager;

  private ResourcePoolsBuilder rPoolsBuilder;

  private BpsService service;

  private String endpoint = "https://webapi.bps.go.id/v1/api/";

  private String key = "06186138228a67b7a37bfb2730be3c2b";

  private String lang = "ind";

  public BpsAdaptee(CacheManager cacheManager) {
    this.cacheManager = cacheManager;
    this.rPoolsBuilder =
        ResourcePoolsBuilder.newResourcePoolsBuilder()
            // .heap(10, EntryUnit.ENTRIES)
            // .offheap(1, MemoryUnit.MB)
            .disk(20, MemoryUnit.MB, true);

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient client =
        new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)
            // .addInterceptor(interceptor)
            .build();

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(client)
            .build();

    service = retrofit.create(BpsService.class);
  }

  /*
   * @param search e.g: [domain_id]-[sub_id]:[query]
   */
  @Override
  public Page<Dataflow> getDataflow(String search, Integer pageSize, Integer page) {
    DataflowQuery query = new DataflowQuery(search);

    try {
      Call<ResponseData> call =
          service.getVariables(key, query.getDomain(), null, null, null, false, page, lang);
      ResponseData responseData = call.execute().body();

      // parse dataflow
      DataflowParser parser = new DataflowParser(query.getDomain());
      parser.setPageSize(pageSize);
      parser.setPage(page);

      return parser.parse(responseData);
    } catch (IOException e) {
      e.printStackTrace();
      PageRequest pageRequest = PageRequest.of(page, pageSize != null ? pageSize : 0);

      return new PageImpl<>(new ArrayList<>(), pageRequest, 0);
    }
  }

  @Override
  public Dataflow getDataflowById(String id) {
    DataId dataId = new DataId(id);

    if (dataId.isValid()) {
      DataContent dataContent = getDataContent(dataId, null);

      if (dataContent.hasVariable()) {
        Variable variable = dataContent.getVar().get(0);

        return new Dataflow(id, variable.getTitle());
      }
    }

    return new Dataflow();
  }

  @Override
  public List<DataSeries> getDataset(String id, Map<String, Object> filter) {
    DataId dataId = new DataId(id);

    if (dataId.isValid()) {
      return (new DatasetParser()).parse(dataId, getDsd(id), getDataContent(dataId, filter));
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public DataStructure getDsd(String id) {
    DataId dataId = new DataId(id);

    if (dataId.isValid()) {
      // preparing cache pool
      Cache<String, DataStructure> cache =
          cacheManager.getCache("bps-dsd", String.class, DataStructure.class);

      if (cache == null) {
        cache =
            cacheManager.createCache(
                "bps-dsd",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(
                    String.class, DataStructure.class, rPoolsBuilder));
      }

      // retrive dsd from cache
      if (cache.containsKey(id)) {
        return cache.get(id);
      }

      // generate dsd from datacontent
      return (new DataStructureParser()).parse(dataId, getDataContent(dataId, null));
    } else {
      return new DataStructure(id);
    }
  }

  public List<Domain> getDomains(String type, String prov) {
    try {
      Call<ResponseData> call = service.getDomains(key, type, prov);
      ResponseData content = call.execute().body();

      return content.getDomains();
    } catch (IOException e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  public DataContent getDataContent(DataId dataId, Map<String, Object> filter) {
    // preparing cache pool
    // cacheManager.removeCache("bps-data");
    Cache<String, DataContent> cache =
        cacheManager.getCache("bps-data", String.class, DataContent.class);

    if (cache == null) {
      cache =
          cacheManager.createCache(
              "bps-data",
              CacheConfigurationBuilder.newCacheConfigurationBuilder(
                  String.class, DataContent.class, rPoolsBuilder));
    }

    // filter by key series
    Map<String, String> keyFilter = new HashMap<>();
    String periodFilter = null;

    if (filter != null) {
      String key = filter.getOrDefault("key", "").toString();
      String[] keySeries = key.split("\\.", -1);
      DataStructure dsd = getDsd(dataId.getId());
      List<Dimension> dimensions = dsd.getSortedDimensionList();

      if (keySeries.length == dimensions.size()) {
        int i = 0;

        for (Dimension dim : dimensions) {
          if (keySeries[i].length() > 0) {
            keyFilter.put(dim.getId(), keySeries[i]);
          }

          i++;
        }
      }

      periodFilter =
          getPeriodFilter(
              filter.getOrDefault("startPeriod", "").toString(),
              filter.getOrDefault("endPeriod", "").toString());
    }

    // retrieve value from cache
    String cacheKey =
        String.join(
            ".",
            dataId.getId(),
            keyFilter.getOrDefault("VAR", ""),
            keyFilter.getOrDefault("AREA", ""),
            keyFilter.getOrDefault("PERIOD", ""),
            periodFilter);

    if (cache.containsKey(cacheKey)) {
      return cache.get(cacheKey);
    }

    // make a web service request
    try {
      Call<DataContent> call =
          service.getData(
              key,
              dataId.getDomain(),
              dataId.getVarId(),
              keyFilter.get("VAR"),
              keyFilter.get("AREA"),
              periodFilter,
              keyFilter.get("PERIOD"),
              lang);

      DataContent dataContent = call.execute().body();
      cache.put(dataId.getId(), dataContent);

      return dataContent;
    } catch (Exception e) {
      e.printStackTrace();
      return new DataContent();
    }
  }

  private String getPeriodFilter(String startPeriod, String endPeriod) {
    StringBuilder builder = new StringBuilder();

    // Start Period
    if (DateTimeUtils.isValidDate(startPeriod)) {
      String year = Integer.toString(DateTime.parse(startPeriod).getYear());
      builder.append(convertYear(year));
    } else {
      builder.append("0");
    }

    builder.append(":");

    // End Period
    if (DateTimeUtils.isValidDate(endPeriod)) {
      String year = Integer.toString(DateTime.parse(endPeriod).getYear());
      builder.append(convertYear(year));
    } else {
      String year = Integer.toString(DateTime.now().getYear());
      builder.append(convertYear(year));
    }

    return builder.toString();
  }

  private String convertYear(String year) {
    StringBuilder builder = new StringBuilder();
    String century = year.substring(0, 2);
    String era = year.substring(2, 4);

    if (century.equals("20")) {
      builder.append("1");
    }

    builder.append(era);

    return builder.toString();
  }
}
