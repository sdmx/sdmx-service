package org.soenda.service.sdmx.datasource.adaptee;

import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.springframework.data.domain.Page;

public interface Adaptee {
  public Page<Dataflow> getDataflow(String search, Integer pageSize, Integer page);

  public Dataflow getDataflowById(String id);

  public List<DataSeries> getDataset(String id, Map<String, Object> filter);

  public DataStructure getDsd(String id);
}
