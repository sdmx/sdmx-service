package org.soenda.service.sdmx.object;

import lombok.Data;

@Data
public class SdmxProvider {

  public static final String WS_TYPE_SOAP = "soap";
  public static final String WS_TYPE_REST = "rest";

  public static final String VERSION_20 = "2.0";
  public static final String VERSION_21 = "2.1";

  private String id;
  private String url;
  private String wsType;
  private String version;

  public SdmxProvider(String id) {
    this.id = id;
  }
}
