package org.soenda.service.sdmx.object;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

@Data
public class Observation implements Serializable {

  private static final long serialVersionUID = 1L;

  private String dimension;

  private DateTime timeDimension;

  private Double value;

  private Map<String, String> attributes = new HashMap<>();

  public Observation() {}

  public Observation(String dimension, String value) {
    setDimension(dimension);
    this.value = value.matches("-?\\d+") ? Double.parseDouble(value) : 0d;
  }

  public Observation(String dimension, Double value) {
    setDimension(dimension);
    this.value = value;
  }

  public String getDimension(String format) {
    try {
      return timeDimension.toString(format);
    } catch (Exception e) {
      e.printStackTrace();
      return dimension;
    }
  }

  public void setDimension(String dimension) {
    try {
      timeDimension = DateTime.parse(dimension, DateTimeFormat.forPattern("MMMyyyy"));
      this.dimension = timeDimension.toString("YYYY-MM");
    } catch (Exception e) {
      //            e.printStackTrace();
      this.dimension = dimension;
    }
  }

  public void setValue(String value) {
    this.value = value.equalsIgnoreCase("NaN") ? 0 : Double.parseDouble(value);
  }

  public void addAttribute(String name, String value) {
    attributes.put(name, value);
  }
}
