package org.soenda.service.sdmx.object;

import java.io.Serializable;
import lombok.Data;

@Data
public class Dataflow implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;

  private String version;

  private String name;

  public Dataflow() {}

  public Dataflow(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public Dataflow(String id, String name, String version) {
    this.id = id;
    this.name = name;
    this.version = version;
  }

  public void setId(String id) {
    if (name == null) {
      this.name = id;
    }

    this.id = id;
  }

  public boolean isEmpty() {
    return id == null;
  }
}
