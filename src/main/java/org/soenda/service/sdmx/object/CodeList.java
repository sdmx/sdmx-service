package org.soenda.service.sdmx.object;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;

@Data
public class CodeList implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;

  private String name;

  private String version;

  private Map<String, String> codes = new LinkedHashMap<>();

  public CodeList() {}

  public CodeList(String id) {
    this.id = id;
  }

  public void addCode(String code, String name) {
    codes.put(code, name);
  }
}
