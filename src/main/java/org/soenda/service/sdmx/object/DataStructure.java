package org.soenda.service.sdmx.object;

import java.io.Serializable;
import java.util.*;
import lombok.Data;

@Data
public class DataStructure implements Serializable {

  private static final long serialVersionUID = -6330037177433871052L;

  private String id;

  private Map<String, Concept> concepts = new HashMap<>();

  private Map<String, CodeList> codeLists = new HashMap<>();

  private Map<String, Dimension> dimensionList = new HashMap<>();

  private Map<String, Concept> measureList = new HashMap<>();

  public DataStructure() {}

  public DataStructure(String id) {
    this.id = id;
  }

  public CodeList getCodeList(String id) {
    return codeLists.get(id);
  }

  public void addCodeList(String id, CodeList code) {
    codeLists.put(id, code);
  }

  public void addMeasure(String id, Concept measure) {
    measureList.put(id, measure);
  }

  public List<Dimension> getSortedDimensionList() {
    List<Dimension> list = new LinkedList<>();

    for (Map.Entry<String, Dimension> item : dimensionList.entrySet()) {
      list.add(item.getValue());
    }

    Collections.sort(list, new SortByPosition());

    return list;
  }

  private class SortByPosition implements Comparator<Dimension> {
    public int compare(Dimension a, Dimension b) {
      return a.getPosition() - b.getPosition();
    }
  }

  public Dimension getDimension(String id) {
    return dimensionList.get(id);
  }

  public void addDimension(String id, Dimension dimension) {
    dimensionList.put(id, dimension);
  }

  public boolean hasDimension(String id) {
    return dimensionList.containsKey(id);
  }

  public Concept getConcept(String id) {
    return concepts.get(id);
  }

  public void addConcept(String id, Concept concept) {
    concepts.put(id, concept);
  }

  public boolean hasConcept(String id) {
    return concepts.containsKey(id);
  }

  public String getDefaultFilterKey() {
    Map<String, String[]> filteredKeys = new HashMap<>();

    for (Map.Entry<String, Dimension> item : dimensionList.entrySet()) {
      if (item.getValue().isTimeDimension()) {
        continue;
      }

      // #dev
      //            if (item.getKey() == null) {
      //                continue;
      //            }

      switch (item.getKey()) {
        case "FREQ":
          filteredKeys.put(item.getKey(), new String[] {"A"});
          break;

        case "GEO":
          filteredKeys.put(item.getKey(), new String[] {""});
          break;

        default:
          Set<String> options = item.getValue().getCodeList().getCodes().keySet();

          if (options != null) {
            // select all
            if (options.size() <= 5) {
              filteredKeys.put(item.getKey(), new String[] {""});
            }
            // set default filter
            else {
              filteredKeys.put(item.getKey(), new String[] {options.toArray()[0].toString()});
            }
          }
          break;
      }
    }

    return buildFilterQuery(filteredKeys);
  }

  public String buildFilterQuery(Map<String, String[]> filteredKeys) {
    StringBuilder sb = new StringBuilder();
    int i = 0;
    int keyCounter = 0;

    for (Dimension dimension : getSortedDimensionList()) {
      if (i > 0) {
        sb.append(".");
      }

      String[] values = filteredKeys.get(dimension.getId());

      if (values != null) {
        StringBuilder key = new StringBuilder();

        for (int j = values.length - 1; j >= 0; j--) {
          if (j < values.length - 1) {
            key.append("+");
          }

          key.append(values[j]);
        }

        sb.append(key);

        if (key.length() > 0) {
          keyCounter++;
        }
      }

      i++;
    }

    return keyCounter > 0 ? sb.toString() : "all";
  }
}
