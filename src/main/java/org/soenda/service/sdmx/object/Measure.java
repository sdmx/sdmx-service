package org.soenda.service.sdmx.object;

import java.io.Serializable;
import java.util.*;
import lombok.Data;

@Data
public class Measure implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;

  private Map<String, Concept> concept = new HashMap<>();

  public Measure() {}

  public Measure(String id) {
    this.id = id;
  }
}
