package org.soenda.service.sdmx.object;

import java.io.Serializable;
import java.util.*;
import lombok.Data;

@Data
public class DataSeries implements Serializable {

  private static final long serialVersionUID = 1L;

  private Map<String, String> keys = new TreeMap<>();

  private Map<String, String> attributes = new HashMap<>();

  private List<Observation> observations = new ArrayList<>();

  public Map<String, String> getKeys() {
    return keys;
  }

  public void addKey(String id, String value) {
    keys.put(id, value);
  }

  public void addAttribute(String id, String value) {
    attributes.put(id, value);
  }

  public void addObservation(Observation observation) {
    observations.add(observation);
  }
}
