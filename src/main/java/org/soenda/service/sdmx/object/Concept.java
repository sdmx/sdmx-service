package org.soenda.service.sdmx.object;

import java.io.Serializable;
import lombok.Data;

@Data
public class Concept implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;

  private String name;

  private String description;

  public Concept() {}

  public Concept(String id) {
    this.id = id;
  }

  public Concept(String id, String name) {
    this.id = id;
    this.name = name;
  }
}
