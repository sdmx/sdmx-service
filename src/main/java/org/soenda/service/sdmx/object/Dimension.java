package org.soenda.service.sdmx.object;

import java.io.Serializable;
import lombok.Data;

@Data
public class Dimension implements Serializable {

  private static final long serialVersionUID = 1L;

  private String id;

  private int position;

  private Concept concept;

  private CodeList codeList;

  private boolean isTimeDimension = false;

  private boolean isPrimaryMeasure = false;

  public Dimension() {}

  public Dimension(String id, int position) {
    this.id = id;
    this.position = position;
  }

  public Dimension(String id, int position, boolean isTimeDimension) {
    this.id = id;
    this.position = position;
    this.isTimeDimension = isTimeDimension;
  }

  public Dimension(String id, String position, boolean isTimeDimension) {
    this.id = id;
    this.position = Integer.parseInt(position);
    this.isTimeDimension = isTimeDimension;
  }

  public boolean isTimeDimension() {
    return isTimeDimension;
  }

  public void setTimeDimension(boolean timeDimension) {
    isTimeDimension = timeDimension;
  }

  public boolean isPrimaryMeasure() {
    return isPrimaryMeasure;
  }

  public void setPrimaryMeasure(boolean primarymeasure) {
    isPrimaryMeasure = primarymeasure;
  }
}
