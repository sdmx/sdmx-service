package org.soenda.service.sdmx.parser;

import java.io.StringReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.soenda.service.sdmx.object.*;
import org.springframework.http.ResponseEntity;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/** Created by Ramdhan on 06/10/2017. */
public class DataStructureParser implements SdmxParser {

  private String lang = "en";

  private InputSource source;

  public DataStructureParser(InputSource source) {
    this.source = source;
  }

  public DataStructureParser(ResponseEntity<String> res) {
    source = new InputSource(new StringReader(res.getBody()));
  }

  public InputSource getSource() {
    return source;
  }

  public DataStructure get() {
    DataStructure dsd = new DataStructure();

    try {
      SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
      DefaultHandler handler =
          new DefaultHandler() {
            private boolean bmeasure = false;

            private boolean bdimension = false;
            private Dimension dimension;

            private boolean bconcept = false;
            private Concept concept;

            private boolean bcodeList = false;
            private CodeList codeList;

            private boolean bname = false;
            private boolean bdescription = false;
            private String code;

            public void startElement(
                String url, String localName, String tagName, Attributes attribute)
                throws SAXException {
              String attrLang = attribute.getValue("xml:lang");
              boolean langExists = attrLang != null;

              if (tagName.equals("str:DataStructure")) {
                dsd.setId(attribute.getValue("id"));
              }

              // CodeList
              if (tagName.equals("str:Codelist")) {
                codeList = new CodeList(attribute.getValue("id"));
                bcodeList = true;
              }

              if (bcodeList) {
                // SeriesKey
                if (tagName.equals("str:Code")) {
                  code = attribute.getValue("id");
                }

                if (tagName.equals("com:Name") && langExists && attrLang.equals(lang)) {
                  bname = true;
                }
              }

              // Concepts
              if (tagName.equals("str:Concept")) {
                concept = new Concept(attribute.getValue("id"));
                bconcept = true;
              }

              if (bconcept && langExists && attrLang.equals(lang)) {
                // SeriesKey
                if (tagName.equals("com:Name")) {
                  bname = true;
                }

                if (tagName.equals("com:Description")) {
                  bdescription = true;
                }
              }

              // Dimension
              if (tagName.equals("str:Dimension") || tagName.equals("str:TimeDimension")) {
                String id = attribute.getValue("id");

                // #dev
                if (id != null) {
                  dimension =
                      new Dimension(
                          attribute.getValue("id"),
                          attribute.getValue("position"),
                          tagName.equals("str:TimeDimension"));
                  bdimension = true;
                }
              }

              if (bdimension && tagName.equals("Ref")) {
                String classRef = attribute.getValue("class");

                if (classRef != null) {
                  String refId = attribute.getValue("id");

                  switch (classRef) {
                    case "Concept":
                      dimension.setConcept(dsd.getConcept(refId));
                      break;

                    case "Codelist":
                      dimension.setCodeList(dsd.getCodeList(refId));
                      break;
                  }
                }
              }

              // Measure
              if (tagName.equals("str:PrimaryMeasure")) {
                code = attribute.getValue("id");
                bmeasure = true;
              }

              if (bmeasure && tagName.equals("Ref")) {
                String refId = attribute.getValue("id");

                if (code != null && dsd.hasConcept(refId)) {
                  // dimension.setPrimaryMeasure(true);
                  dsd.addMeasure(code, dsd.getConcept(refId));
                }

                code = null;
              }
            }

            public void characters(char ch[], int start, int length) throws SAXException {
              String content = new String(ch, start, length);

              if (bcodeList && bname) {
                if (code != null) {
                  codeList.addCode(code, content);
                } else {
                  codeList.setName(content);
                }

                bname = false;
              }

              if (bconcept) {
                if (bname) {
                  concept.setName(content);
                  bname = false;
                }

                if (bdescription) {
                  concept.setDescription(content);
                  bdescription = false;
                }
              }
            }

            public void endElement(String uri, String localName, String tagName)
                throws SAXException {
              if (bcodeList) {
                if (tagName.equals("str:Code")) {
                  code = null;
                }

                if (tagName.equals("str:Codelist")) {
                  dsd.addCodeList(codeList.getId(), codeList);
                  bcodeList = false;
                }
              }

              if (bconcept && tagName.equals("str:Concept")) {
                dsd.addConcept(concept.getId(), concept);
                bconcept = false;
              }

              if (bdimension
                  && (tagName.equals("str:Dimension") || tagName.equals("str:TimeDimension"))) {
                dsd.addDimension(dimension.getId(), dimension);
                bdimension = false;
              }
            }
          };

      parser.parse(source, handler);

      return dsd;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
