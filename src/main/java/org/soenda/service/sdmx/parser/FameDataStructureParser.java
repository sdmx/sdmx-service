package org.soenda.service.sdmx.parser;

import java.io.StringReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.soenda.service.sdmx.object.CodeList;
import org.soenda.service.sdmx.object.Concept;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dimension;
import org.springframework.http.ResponseEntity;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/** Created by Ramdhan on 06/10/2017. */
public class FameDataStructureParser implements SdmxParser {

  private String lang = "en";

  private InputSource source;

  public FameDataStructureParser(InputSource source) {
    this.source = source;
  }

  public FameDataStructureParser(ResponseEntity<String> res) {
    source = new InputSource(new StringReader(res.getBody()));
  }

  public InputSource getSource() {
    return source;
  }

  public DataStructure get() {
    DataStructure dsd = new DataStructure();

    try {
      SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
      DefaultHandler handler =
          new DefaultHandler() {
            private boolean bmeasure = false;

            private boolean bdimensionList = false;
            private boolean bdimension = false;
            private boolean bisTimedimension = false;
            private boolean bdimensionConcept = false;
            private boolean bdimensionEnumeration = false;
            private int dimensionCounter = 0;
            private Dimension dimension;

            private boolean bconcept = false;
            private Concept concept;

            private boolean bcodeList = false;
            private CodeList codeList;

            private boolean bname = false;
            private boolean bdescription = false;
            private String code;

            public void startElement(
                String url, String localName, String tagName, Attributes attribute)
                throws SAXException {
              String attrLang = attribute.getValue("xml:lang");
              boolean langExists = attrLang != null;

              if (tagName.equals("structure:DataStructure")) {
                dsd.setId(attribute.getValue("id"));
              }

              // CodeList
              if (tagName.equals("structure:Codelist")) {
                codeList = new CodeList(attribute.getValue("id"));
                bcodeList = true;
              }

              if (bcodeList) {
                // SeriesKey
                if (tagName.equals("structure:Code")) {
                  code = attribute.getValue("id");
                }

                if (tagName.equals("common:Name") && langExists && attrLang.equals(lang)) {
                  bname = true;
                }
              }

              // Concepts
              if (tagName.equals("structure:Concept")) {
                concept = new Concept(attribute.getValue("id"));
                bconcept = true;
              }

              if (bconcept && langExists && attrLang.equals(lang)) {
                // SeriesKey
                if (tagName.equals("common:Name")) {
                  bname = true;
                }

                if (tagName.equals("common:Description")) {
                  bdescription = true;
                }
              }

              // Dimensions
              if (tagName.equals("structure:DimensionList")) {
                bdimensionList = true;
              }

              if (bdimensionList
                  && (tagName.equals("structure:Dimension")
                      || tagName.equals("structure:TimeDimension"))) {
                bisTimedimension = tagName.equals("structure:TimeDimension");
                bdimension = true;
              }

              // Concept
              if (bdimension && tagName.equals("structure:ConceptIdentity")) {
                bdimensionConcept = true;
              }

              if (bdimensionConcept && tagName.equals("Ref")) {
                String refId = attribute.getValue("id");
                dimension =
                    new Dimension(refId, Integer.toString(dimensionCounter++), bisTimedimension);
                dimension.setConcept(dsd.getConcept(refId));
              }

              // Code List
              if (bdimension && tagName.equals("structure:Enumeration")) {
                bdimensionEnumeration = true;
              }

              if (bdimensionEnumeration && tagName.equals("Ref")) {
                String refId = attribute.getValue("id");
                dimension.setCodeList(dsd.getCodeList(refId));
              }

              // Measure
              if (tagName.equals("structure:PrimaryMeasure")) {
                code = attribute.getValue("id");
                bmeasure = true;
              }

              if (bmeasure && tagName.equals("Ref")) {
                String refId = attribute.getValue("id");

                if (code != null && dsd.hasConcept(refId)) {
                  // dimension.setPrimaryMeasure(true);
                  dsd.addMeasure(code, dsd.getConcept(refId));
                }

                code = null;
              }
            }

            public void characters(char ch[], int start, int length) throws SAXException {
              String content = new String(ch, start, length);

              if (bcodeList && bname) {
                if (code != null) {
                  codeList.addCode(code, content);
                } else {
                  codeList.setName(content);
                }

                bname = false;
              }

              if (bconcept) {
                if (bname) {
                  concept.setName(content);
                  bname = false;
                }

                if (bdescription) {
                  concept.setDescription(content);
                  bdescription = false;
                }
              }
            }

            public void endElement(String uri, String localName, String tagName)
                throws SAXException {
              if (bcodeList) {
                if (tagName.equals("structure:Code")) {
                  code = null;
                }

                if (tagName.equals("structure:Codelist")) {
                  dsd.addCodeList(codeList.getId(), codeList);
                  bcodeList = false;
                }
              }

              if (bconcept && tagName.equals("structure:Concept")) {
                dsd.addConcept(concept.getId(), concept);
                bconcept = false;
              }

              if (bdimensionList) {
                if (bdimension) {
                  if (bdimensionConcept && tagName.equals("structure:ConceptIdentity")) {
                    bdimensionConcept = false;
                  }

                  if (bdimensionEnumeration && tagName.equals("structure:Enumeration")) {
                    bdimensionEnumeration = false;
                  }

                  if (tagName.equals("structure:Dimension")
                      || tagName.equals("structure:TimeDimension")) {
                    dsd.addDimension(dimension.getId(), dimension);
                    bdimension = false;
                  }
                }

                if (tagName.equals("structure:DimensionList")) {
                  bdimensionList = false;
                }
              }
            }
          };

      parser.parse(source, handler);

      return dsd;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
