package org.soenda.service.sdmx.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.soenda.service.connector.fame.object.CustomTiq;
import org.soenda.service.connector.fame.object.FameObject;
import org.soenda.service.sdmx.object.*;

public class FameParser {

  public FameParser() {}

  public List<Dataflow> parseDataflow(List<FameObject> list) {
    List<Dataflow> dataflow = new ArrayList<>();

    for (FameObject item : list) {
      dataflow.add(parseDataflow(item));
    }

    return dataflow;
  }

  public Dataflow parseDataflow(FameObject obj) {
    return new Dataflow(obj.getObjectName(), obj.getDescription());
  }

  public List<DataSeries> parseDataset(List<CustomTiq> list, DataStructure dsd) {
    List<DataSeries> dataSeries = new ArrayList<>();

    for (CustomTiq tiqObject : list) {
      DataSeries series = parseDataset(tiqObject);
      String[] keys = tiqObject.getName().split("\\.");

      for (Map.Entry<String, Dimension> dimItem : dsd.getDimensionList().entrySet()) {
        Dimension dim = dimItem.getValue();

        if (dim.isTimeDimension()) {
          continue;
        }

        series.addKey(dimItem.getKey(), keys[dim.getPosition() + 1]);
      }

      dataSeries.add(series);
    }

    return dataSeries;
  }

  public DataSeries parseDataset(CustomTiq obj) {
    DataSeries dataSeries = new DataSeries();

    // Observations
    for (org.soenda.service.connector.fame.object.Observation obs : obj.getObservations()) {
      dataSeries.addObservation(
          new Observation(obs.getDateIndex(), obs.getObservationValue().toString()));
    }

    return dataSeries;
  }
}
