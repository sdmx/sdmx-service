package org.soenda.service.sdmx.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;

public class SeriesParser {

  private static SeriesParser instance;

  private SeriesParser() {}

  public static SeriesParser getInstance() {
    if (instance == null) {
      instance = new SeriesParser();
    }

    return instance;
  }

  public List<Map<String, Object>> parse(List<DataSeries> list) {
    List<Map<String, Object>> series = new ArrayList<>();

    if (list != null && list.size() > 0) {
      for (DataSeries item : list) {
        series.addAll(parse(item));
      }
    }

    return series;
  }

  public List<Map<String, Object>> parse(DataSeries dataSeries) {
    List<Map<String, Object>> series = new ArrayList<>();

    for (Observation obs : dataSeries.getObservations()) {
      Map<String, Object> object = new HashMap<>(dataSeries.getKeys());
      object.put("OBS_DIMENSION", obs.getDimension());
      object.put("OBS_VALUE", obs.getValue());

      series.add(object);
    }

    return series;
  }
}
