package org.soenda.service.sdmx.parser.bps;

import java.util.ArrayList;
import java.util.List;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Observation;
import org.soenda.service.sdmx.parser.bps.object.DataId;

public class DatasetParser {

  public DatasetParser() {}

  public List<DataSeries> parse(DataId dataId, DataStructure dsd, DataContent dataContent) {
    List<DataSeries> series = new ArrayList<DataSeries>();

    for (VerticalVariable vervar : dataContent.getVervar()) {
      for (DerivedVariable turvar : dataContent.getTurvar()) {
        for (PeriodData tahun : dataContent.getTahun()) {
          for (DerivedPeriodData turtahun : dataContent.getTurtahun()) {
            DataSeries dataSeries = new DataSeries();

            // AREA - vervar
            if (dsd.hasDimension("AREA")) {
              dataSeries.addKey("AREA", vervar.getKodeVerId());
            }

            // VAR - turvar
            if (dsd.hasDimension("VAR")) {
              dataSeries.addKey("VAR", turvar.getTurvarId());
            }

            // PERIOD - turtahun
            if (dsd.hasDimension("PERIOD")) {
              dataSeries.addKey("PERIOD", turtahun.getTurthId());
            }

            Double value =
                dataContent.getValue(
                    vervar.getKodeVerId(),
                    dataId.getVarId(),
                    turvar.getTurvarId(),
                    tahun.getThId(),
                    turtahun.getTurthId());

            dataSeries.addKey("TIME_PERIOD", tahun.getTh());
            dataSeries.addKey("OBS_VALUE", Double.toString(value));
            dataSeries.addObservation(new Observation(tahun.getTh(), value));
            series.add(dataSeries);
          }
        }
      }
    }

    return series;
  }
}
