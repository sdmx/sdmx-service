package org.soenda.service.sdmx.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Joiner;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class SdmxParserException extends Exception {

  private int code = 500;

  private String severity = "Information";

  private List<String> messages = new ArrayList<>();

  public SdmxParserException(int code, String severity, String... messages) {
    this.code = code;
    this.severity = severity;

    if (messages != null) {
      for (String message : messages) {
        addMessage(message);
      }
    }
  }

  public SdmxParserException(String... messages) {
    for (String message : messages) {
      addMessage(message);
    }
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public List<String> getMessages() {
    return messages;
  }

  public void setMessages(List<String> messages) {
    this.messages = messages;
  }

  public void addMessage(String message) {
    messages.add(message);
  }

  @Override
  public String getMessage() {
    return Joiner.on("\n").join(messages);
  }

  public ResponseEntity<String> getResponseEntity() {
    ObjectMapper mapper = new ObjectMapper();
    ObjectNode response = mapper.createObjectNode();
    response.put("code", code);
    response.put("severity", severity);

    ArrayNode messages = mapper.createArrayNode();

    for (String message : this.messages) {
      messages.add(message);
    }

    response.putPOJO("messages", messages);

    return new ResponseEntity<>(response.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
