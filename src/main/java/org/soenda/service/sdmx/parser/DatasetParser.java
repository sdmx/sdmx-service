package org.soenda.service.sdmx.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;
import org.springframework.http.ResponseEntity;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/** Created by Ramdhan on 06/10/2017. */
public class DatasetParser implements SdmxParser {

  private String lang = "en";

  private InputSource source;

  private SdmxParserException exception;

  public DatasetParser(InputSource source) {
    this.source = source;
  }

  public DatasetParser(ResponseEntity<String> res) {
    source = new InputSource(new StringReader(res.getBody()));
  }

  public InputSource getSource() {
    return source;
  }

  public List<DataSeries> getData() throws SdmxParserException {
    List<DataSeries> data = new ArrayList<>();

    try {
      SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
      DefaultHandler handler =
          new DefaultHandler() {
            private boolean bseries = false;
            private boolean bseriesKey = false;
            private boolean battributes = false;
            private DataSeries dataSeries;

            private boolean bobs = false;
            private Observation obs;

            private boolean bmessage = false;
            private boolean bmessageText = false;

            public void startElement(
                String url, String localName, String tagName, Attributes attribute)
                throws SAXException {
              if (tagName.equalsIgnoreCase("generic:Series")) {
                dataSeries = new DataSeries();
                bseries = true;
              }

              // data series
              if (bseries) {
                // SeriesKey
                if (tagName.equalsIgnoreCase("generic:SeriesKey")) {
                  bseriesKey = true;
                }

                if (bseriesKey && tagName.equalsIgnoreCase("generic:Value")) {
                  dataSeries.addKey(attribute.getValue("id"), attribute.getValue("value"));
                }

                // Attributes
                if (!bobs && tagName.equalsIgnoreCase("generic:Attributes")) {
                  battributes = true;
                }

                if (battributes && tagName.equalsIgnoreCase("generic:Value")) {
                  dataSeries.addAttribute(attribute.getValue("id"), attribute.getValue("value"));
                }

                // Observations
                if (tagName.equalsIgnoreCase("generic:Obs")) {
                  obs = new Observation();
                  bobs = true;
                }

                if (bobs) {
                  if (tagName.equalsIgnoreCase("generic:ObsDimension")) {
                    obs.setDimension(attribute.getValue("value"));
                  }

                  if (tagName.equalsIgnoreCase("generic:ObsValue")) {
                    obs.setValue(attribute.getValue("value"));
                  }
                }
              }

              // message response
              if (exception == null && tagName.equalsIgnoreCase("footer:Message")) {
                //                        exception = new SdmxParserException(
                //                            Integer.parseInt(attribute.getValue("code")),
                //                            attribute.getValue("severity")
                //                        );
                bmessage = true;
              }

              if (bmessage) {
                if (tagName.equalsIgnoreCase("common:Text")
                    && attribute.getValue("xml:lang").equals("en")) {
                  bmessageText = true;
                }
              }
            }

            public void characters(char ch[], int start, int length) throws SAXException {
              if (bmessageText) {
                if (exception != null) {
                  exception.addMessage(new String(ch, start, length));
                }

                bmessageText = false;
              }
            }

            public void endElement(String uri, String localName, String tagName)
                throws SAXException {
              if (bobs && tagName.equalsIgnoreCase("generic:Obs")) {
                dataSeries.addObservation(obs);
                bobs = false;
              }

              if (bmessage && tagName.equalsIgnoreCase("footer:Message")) {
                bmessage = false;
              }

              if (bseriesKey && tagName.equalsIgnoreCase("generic:SeriesKey")) {
                bseriesKey = false;
              }

              if (bseries && tagName.equalsIgnoreCase("generic:Series")) {
                data.add(dataSeries);
                bseries = false;
              }
            }
          };

      parser.parse(source, handler);

      if (exception != null) {
        throw exception;
      }

      return data;
    } catch (SdmxParserException e) {
      e.printStackTrace();
      //            throw e;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public List<Map<String, Object>> getDataMap() throws SdmxParserException {
    return SeriesParser.getInstance().parse(getData());
  }
}
