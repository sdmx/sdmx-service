package org.soenda.service.sdmx.parser.bps;

import java.io.IOException;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.sdmx.object.*;
import org.soenda.service.sdmx.parser.bps.object.DataId;

public class DataStructureParser {

  public DataStructureParser() {}

  public DataStructure parse(DataId dataId, DataContent dataContent) {
    DataStructure dsd = new DataStructure(dataId.getId());

    // mapping dsd
    try {
      int dimPos = 0;

      setTurvarDimension(dsd, dataContent, dimPos++); // VAR
      setVervarDimension(dsd, dataContent, dimPos++); // AREA
      setTurtahunDimension(dsd, dataContent, dimPos++); // PERIOD
      // setTahunDimension(dsd, dataContent, dimPos++); // TIME_PERIOD
    } catch (IOException e) {
      e.printStackTrace();
    }

    return dsd;
  }

  private void setTurvarDimension(DataStructure dsd, DataContent dataContent, int position)
      throws IOException {
    Concept concept = new Concept("VAR", "Variable");
    concept.setDescription(concept.getName());

    CodeList codeList = new CodeList(concept.getId());
    codeList.setName(concept.getName());

    // collecting code list
    for (DerivedVariable item : dataContent.getTurvar()) {
      if (!item.getTurvarId().equals("0")) {
        codeList.addCode(item.getTurvarId(), item.getTurvar());
      }
    }

    // assign dimension into dsd
    if (codeList.getCodes().size() > 0) {
      assignDimension(dsd, concept, codeList, position);
    }
  }

  private void setVervarDimension(DataStructure dsd, DataContent dataContent, int position) {
    Concept concept = new Concept("AREA", "Area");
    concept.setDescription(concept.getName());

    CodeList codeList = new CodeList(concept.getId());
    codeList.setName(concept.getName());

    // collecting code list
    for (VerticalVariable item : dataContent.getVervar()) {
      if (!item.getKodeVerId().equals("0")) {
        codeList.addCode(item.getKodeVerId(), item.getVervar());
      }
    }

    // assign dimension into dsd
    if (codeList.getCodes().size() > 0) {
      assignDimension(dsd, concept, codeList, position);
    }
  }

  private void setTurtahunDimension(DataStructure dsd, DataContent dataContent, int position) {
    Concept concept = new Concept("PERIOD", "Period");
    concept.setDescription(concept.getName());

    CodeList codeList = new CodeList(concept.getId());
    codeList.setName(concept.getName());

    // collecting code list
    for (DerivedPeriodData item : dataContent.getTurtahun()) {
      if (!item.getTurthId().equals("0")) {
        codeList.addCode(item.getTurthId(), item.getTurth());
      }
    }

    // assign dimension into dsd
    if (codeList.getCodes().size() > 0) {
      assignDimension(dsd, concept, codeList, position);
    }
  }

  private void setTahunDimension(DataStructure dsd, DataContent dataContent, int position) {
    // concept
    Concept concept = new Concept("TIME_PERIOD", "Time Period");
    concept.setDescription(concept.getName());
    dsd.addConcept(concept.getId(), concept);

    // dimension
    Dimension dimension = new Dimension(concept.getId(), position, true);
    dimension.setConcept(concept);
    dsd.addDimension(dimension.getId(), dimension);
  }

  private void assignDimension(
      DataStructure dsd, Concept concept, CodeList codeList, int position) {
    dsd.addConcept(concept.getId(), concept);
    dsd.addCodeList(codeList.getId(), codeList);

    Dimension dimension = new Dimension(concept.getId(), position);
    dimension.setConcept(concept);
    dimension.setCodeList(codeList);
    dsd.addDimension(dimension.getId(), dimension);
  }
}
