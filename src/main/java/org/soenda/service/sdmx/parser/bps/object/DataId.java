package org.soenda.service.sdmx.parser.bps.object;

import lombok.Data;

@Data
public class DataId {
  private String id;

  private String domain;

  private String varId;

  public DataId(String domain, String varId) {
    this.domain = domain;
    this.varId = varId;
    this.id = getId(domain, varId);
  }

  public DataId(String id) {
    this.id = id;

    if (id != null && id.length() > 0) {
      String[] args = id.split("\\.");
      this.domain = args[0];

      if (args.length > 1) {
        this.varId = args[1];
      }
    }
  }

  public boolean isValid() {
    return domain != null && varId != null;
  }

  public static String getId(String domain, String varId) {
    return String.format("%s.%s", domain, varId);
  }
}
