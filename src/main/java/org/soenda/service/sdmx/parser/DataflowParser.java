package org.soenda.service.sdmx.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.soenda.service.sdmx.object.Dataflow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/** Created by Ramdhan on 06/10/2017. */
public class DataflowParser implements SdmxParser {

  private String lang = "en";

  private InputSource source;

  private final int[] dataCount = {
    0, // whole counter
    0 // displayed counter
  };

  public DataflowParser(InputSource source) {
    this.source = source;
  }

  public DataflowParser(ResponseEntity<String> res) {
    source = new InputSource(new StringReader(res.getBody()));
  }

  public Dataflow one() {
    List<Dataflow> data = all();
    return data.size() > 0 ? data.get(0) : null;
  }

  public List<Dataflow> all() {
    return fetch(null, 0, 0);
  }

  public Page<Dataflow> paginate(String query, int pageRecordSize, int page) {
    return new PageImpl<>(
        fetch(query, pageRecordSize, (page - 1) * pageRecordSize),
        PageRequest.of(page, pageRecordSize),
        dataCount[0]);
  }

  public List<Dataflow> fetch(String query, int pageRecordSize, int pageRecordOffset) {
    List<Dataflow> dataflows = new ArrayList<>();

    // reset data counter
    dataCount[0] = 0;
    dataCount[1] = 0;

    try {
      SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
      DefaultHandler handler =
          new DefaultHandler() {
            private String tmpDfId;

            private boolean bdf;

            public void startElement(String url, String localName, String tagName, Attributes attr)
                throws SAXException {
              if (tagName.equalsIgnoreCase("str:Dataflow")) {
                tmpDfId = attr.getValue("id");
              }

              if (tagName.equalsIgnoreCase("com:Name") && attr.getValue("xml:lang").equals(lang)) {
                bdf = true;
              }
            }

            public void characters(char ch[], int start, int length) throws SAXException {
              if (bdf) {
                String dfName = new String(ch, start, length);

                if (query == null || dfName.toLowerCase().contains(query.toLowerCase())) {
                  if (pageRecordSize == 0
                      || (dataCount[0] > pageRecordOffset && dataCount[1] < pageRecordSize)) {
                    dataflows.add(new Dataflow(tmpDfId, dfName));
                    dataCount[1]++;
                  }

                  dataCount[0]++;
                  bdf = false;
                }
              }
            }
          };

      parser.parse(source, handler);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return dataflows;
  }
}
