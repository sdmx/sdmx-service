package org.soenda.service.sdmx.parser.bps;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.sdmx.object.Dataflow;
import org.soenda.service.sdmx.parser.bps.object.DataId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class DataflowParser {

  @Getter @Setter private Integer page = 1;

  @Getter @Setter private Integer pageSize = 10;

  @Getter @Setter private String domain;

  public DataflowParser(String domain) {
    this.domain = domain;
  }

  public Page<Dataflow> parse(ResponseData response) {
    List<Dataflow> data = new ArrayList<>();

    for (Variable item : response.getVariables()) {
      data.add(new Dataflow(DataId.getId(domain, item.getVarId()), item.getTitle()));
    }

    // implement pagination
    Pagination pagination = response.getPagination();
    PageRequest pageRequest = PageRequest.of(page, pagination.getPerPage());
    int total = pagination.getTotal();

    return new PageImpl<>(data, pageRequest, total);
  }
}
