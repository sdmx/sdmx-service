package org.soenda.service.sdmx.parser.bps.object;

import lombok.Data;

@Data
public class DataflowQuery {

  private String query;

  private String domain = "0000";

  private String subject;

  private String search;

  public DataflowQuery(String domain, String subject, String search) {
    this.domain = domain;
    this.subject = subject;
    this.search = search;
    this.query = getQuery(domain, subject, search);
  }

  public DataflowQuery(String query) {
    this.query = query;

    if (query != null && query.length() > 0) {
      String[] args = query.split("\\:");
      String[] category = args[0].split("\\-");

      setDomain(category[0]);

      if (category.length > 1) {
        setSubject(category[1]);
      }

      if (args.length > 1) {
        setSearch(args[1]);
      }
    }
  }

  public boolean isValid() {
    return domain != null;
  }

  public static String getQuery(String domain, String subject, String search) {
    StringBuilder builder = new StringBuilder(domain);

    if (subject != null && subject.length() > 0) {
      builder.append("-");
      builder.append(subject);
    }

    if (search != null && search.length() > 0) {
      builder.append(":");
      builder.append(search);
    }

    return builder.toString();
  }
}
