package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.List;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.support.util.CollectionUtils;
import org.soenda.service.support.util.IOUtils;

public class JsonAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    return IOUtils.filePut(dest, CollectionUtils.toJson(dataset));
  }
}
