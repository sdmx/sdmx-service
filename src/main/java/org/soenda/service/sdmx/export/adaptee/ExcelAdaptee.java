package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.*;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;
import org.soenda.service.support.export.ExportFactory;
import org.soenda.service.support.export.product.ExcelProduct;

public class ExcelAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    ExcelProduct excelProduct = (ExcelProduct) ExportFactory.create("xls", dest);
    excelProduct.createSheet("data", null);

    int i = 0;

    for (DataSeries series : dataset) {
      Map<String, String> keys = series.getKeys();

      if (i == 0) {
        List<String> header = new ArrayList<>();
        header.add("No");
        header.addAll(keys.keySet());
        header.add("OBS_DIMENSION");
        header.add("OBS_VALUE");
        excelProduct.addItem(header);
      }

      for (Observation obs : series.getObservations()) {
        LinkedList<Object> row = new LinkedList<>(keys.values());
        row.add(obs.getDimension());
        row.add(obs.getValue().toString());
        row.addFirst(++i);
        excelProduct.addItem(row);
      }
    }

    excelProduct.close();

    return new File(dest);
  }
}
