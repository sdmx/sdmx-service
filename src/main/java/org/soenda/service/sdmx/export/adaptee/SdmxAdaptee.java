package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;

public class SdmxAdaptee implements ExportAdaptee {

  private Map<String, Object> params;

  public SdmxAdaptee(Map<String, Object> params) {
    this.params = params;
  }

  private void el(XMLStreamWriter writer, String tagName, String value) throws Exception {
    writer.writeStartElement(tagName);
    writer.writeCharacters(value);
    writer.writeEndElement(); // end Test
  }

  public File exportGenericData(List<DataSeries> dataset, String dest) {
    String id = getParam("id");
    String agencyId = "BI";
    String version = getParam("version").equals("2.0") ? "v2_0" : "v2_1";

    try {
      FileWriter fw = new FileWriter(dest);
      XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fw);

      writer.writeStartDocument();
      writer.writeStartElement("message:GenericData");
      writer.writeAttribute(
          "xmlns:message", "http://www.sdmx.org/resources/sdmxml/schemas/" + version + "/message");
      writer.writeAttribute(
          "xmlns:common", "http://www.sdmx.org/resources/sdmxml/schemas/" + version + "/common");
      writer.writeAttribute(
          "xmlns:generic", "http://www.sdmx.org/resources/sdmxml/schemas/" + version + "/generic");
      writer.writeAttribute("xmlns:xml", "http://www.w3.org/XML/1998/namespace");

      // Header
      writer.writeStartElement("message:Header");
      el(writer, "message:ID", id);
      el(writer, "message:Test", "false");
      el(writer, "message:Prepared", DateTime.now().toString("yyyy-MM-dd'T'HH:mm:ss"));

      // Sender
      writer.writeStartElement("message:Header");
      writer.writeAttribute("id", agencyId);
      // Name
      writer.writeStartElement("common:Name");
      writer.writeAttribute("xml:lang", "en");
      writer.writeCharacters("Bank Indonesia");
      writer.writeEndElement(); // Name
      el(writer, "message:Timezone", getCurrentTimeZoneOffset());
      writer.writeEndElement(); // Sender

      // Receiver
      writer.writeStartElement("message:Receiver");
      writer.writeAttribute("id", "RECEIVER");
      writer.writeEndElement(); // Receiver

      // Structure
      writer.writeStartElement("message:Structure");
      writer.writeAttribute("structureID", id);
      writer.writeAttribute("dimensionAtObservation", "TIME_PERIOD");
      // common:Structure
      writer.writeStartElement("common:Structure");
      // Ref
      writer.writeStartElement("Ref");
      writer.writeAttribute("agencyId", agencyId);
      writer.writeAttribute("id", id);
      writer.writeAttribute("version", "1.0");
      writer.writeEndElement(); // Ref
      writer.writeEndElement(); // common:Structure
      writer.writeEndElement(); // Structure

      el(writer, "message:DataSetAction", "Append");
      el(writer, "message:DataSetID", id);
      writer.writeEndElement(); // Header

      // DataSet
      writer.writeStartElement("message:DataSet");
      writer.writeAttribute("structureRef", id);

      // Series
      for (DataSeries series : dataset) {
        writer.writeStartElement("generic:Series");
        // Series Key
        writer.writeStartElement("generic:SeriesKey");
        // Value
        for (Map.Entry<String, String> attr : series.getKeys().entrySet()) {
          writer.writeStartElement("generic:Value");
          writer.writeAttribute("id", attr.getKey());
          writer.writeAttribute("value", attr.getValue());
          writer.writeEndElement(); // Value
        }
        writer.writeEndElement();

        // Observations
        for (Observation obs : series.getObservations()) {
          writer.writeStartElement("generic:Obs");
          // Dimension
          writer.writeStartElement("generic:ObsDimension");
          writer.writeAttribute("value", obs.getDimension());
          writer.writeEndElement();
          // Value
          writer.writeStartElement("generic:ObsValue");
          writer.writeAttribute("value", obs.getValue().toString());
          writer.writeEndElement();
          // Attributes
          for (Map.Entry<String, String> attr : obs.getAttributes().entrySet()) {
            writer.writeStartElement("generic:Attributes");
            // Value
            writer.writeStartElement("generic:Value");
            writer.writeAttribute("id", attr.getKey());
            writer.writeAttribute("value", attr.getValue());
            writer.writeEndElement(); // Value
            writer.writeEndElement();
          }
          writer.writeEndElement();
        }

        writer.writeEndElement();
      }

      writer.writeEndElement(); // Header

      writer.writeEndElement();
      writer.writeEndDocument();

      writer.flush();
      writer.close();

      fw.close();

      return new File(dest);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    return exportGenericData(dataset, dest);
  }

  public static String getCurrentTimeZoneOffset() {
    DateTimeZone tz = DateTimeZone.getDefault();
    Long instant = DateTime.now().getMillis();

    long offsetInMilliseconds = tz.getOffset(instant);
    long hours = TimeUnit.MILLISECONDS.toHours(offsetInMilliseconds);
    long minutes = TimeUnit.MILLISECONDS.toMinutes(offsetInMilliseconds % 3600);

    return String.format("%02d:%02d", hours, minutes);
  }

  private String getParam(String name) {
    return params.getOrDefault(name, "").toString();
  }
}
