package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import org.apache.logging.log4j.util.Strings;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;

public class SdmxEdiAdaptee implements ExportAdaptee {

  public SdmxEdiAdaptee() {}

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    File file = new File(dest);
    RandomAccessFile stream = new RandomAccessFile(file, "rw");
    FileChannel channel = stream.getChannel();

    // Header
    String id = "MREF000001";
    String dataId = "BI";
    String agencyId = "BI";
    DateTime prepared = DateTime.now();
    String preparedDate = prepared.toString("YYMMDD");
    String preparedTime = prepared.toString("HHmm");

    write(channel, "UNA:+.? '");
    write(
        channel,
        String.format(
            "UNB+UNOC:3+%s+RECEIVER+%s:%s+%s++SDMX-EDI'",
            agencyId, preparedDate, preparedTime, dataId));
    write(channel, String.format("UNH+%s+GESMES:2:1:E6'", id));
    write(channel, "BGM+74'");
    write(channel, String.format("NAD+Z02+%s'", agencyId));
    write(channel, "NAD+MR+RECEIVER'");
    write(channel, String.format("NAD+MS+%s'", agencyId));
    write(channel, String.format("DSI+%s'", dataId));
    write(channel, "STS+3+7'");
    write(channel, String.format("DTM+242:%s:203'", prepared.toString("YYMMDDHHmm")));
    write(channel, String.format("IDE+5+%s'", dataId));
    write(channel, "GIS+AR3'");
    write(channel, "GIS+1:::-'");

    for (DataSeries series : dataset) {
      List<String> keySeries = new ArrayList<>();

      for (Map.Entry<String, String> i : series.getKeys().entrySet()) {
        keySeries.add(i.getValue());
      }

      String firstObsDim = null;
      String lastObsDim = null;
      List<Observation> observations = series.getObservations();

      int obsSize = observations.size();
      int i = 1;
      List<Double> obsValues = new ArrayList<>();

      for (Observation obs : observations) {
        if (firstObsDim == null) {
          firstObsDim = obs.getDimension("yyyyMM");
        } else if (i == obsSize) {
          lastObsDim = obs.getDimension("yyyyMM");
        }

        obsValues.add(obs.getValue());
        i++;
      }

      keySeries.add(firstObsDim + lastObsDim);
      keySeries.add("702");

      //            Collections.reverse(obsValues);
      keySeries.add(Strings.join(obsValues, '+'));

      write(channel, String.format("ARR++%s'", Strings.join(keySeries, ':')));
    }

    write(channel, "FNS+Attributes:10'");
    write(channel, "REL+Z01+5'");

    write(channel, String.format("UNT+90+%s'", id));
    write(channel, String.format("UNZ+1+%s'", dataId));

    stream.close();
    channel.close();

    // verify
    RandomAccessFile reader = new RandomAccessFile(file, "r");

    System.out.println(DateTime.parse("Dec2011", DateTimeFormat.forPattern("MMMyyyy")));
    System.out.println(reader.readLine());
    reader.close();

    return file;
  }

  private void write(FileChannel channel, String value) throws Exception {
    byte[] byteVal = (value + "\n").getBytes();
    ByteBuffer buffer = ByteBuffer.allocate(byteVal.length);
    buffer.put(byteVal);
    buffer.flip();

    channel.write(buffer);
  }
}
