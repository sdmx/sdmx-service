package org.soenda.service.sdmx.export.adaptee;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;

public class PdfAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    Document document = new Document();
    PdfWriter.getInstance(document, new FileOutputStream(dest));
    document.open();

    if (dataset.size() == 0) {
      Chunk chunk = new Chunk("Empty");
      document.add(chunk);
    } else {
      PdfPTable table = null;

      for (DataSeries series : dataset) {
        // draw header table first
        if (table == null) {
          List<String> keys = new ArrayList<>(series.getKeys().keySet());
          keys.add("OBS_DIMENSION");
          keys.add("OBS_VALUE");
          table = new PdfPTable(keys.size());

          drawHeader(table, keys);
        }

        // draw values
        for (Observation obs : series.getObservations()) {
          for (Map.Entry<String, String> key : series.getKeys().entrySet()) {
            table.addCell(key.getValue());
          }

          table.addCell(obs.getDimension());
          table.addCell(obs.getValue().toString());
        }
      }

      document.add(table);
    }

    document.close();

    return new File(dest);
  }

  private void drawHeader(PdfPTable table, Collection<String> keys) {
    for (String key : keys) {
      PdfPCell header = new PdfPCell();
      header.setBackgroundColor(BaseColor.LIGHT_GRAY);
      header.setPhrase(new Phrase(key));
      header.setBorderWidth(2);
      table.addCell(header);
    }
  }
}
