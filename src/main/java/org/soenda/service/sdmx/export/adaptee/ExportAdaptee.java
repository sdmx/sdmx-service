package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.List;
import org.soenda.service.sdmx.object.DataSeries;

public interface ExportAdaptee {
  File export(List<DataSeries> dataset, String dest) throws Exception;
}
