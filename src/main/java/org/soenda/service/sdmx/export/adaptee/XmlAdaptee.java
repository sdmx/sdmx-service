package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;

public class XmlAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    FileWriter fw = new FileWriter(dest);
    XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fw);

    writer.writeStartDocument();
    writer.writeStartElement("data");

    for (DataSeries series : dataset) {
      for (Observation obs : series.getObservations()) {
        writer.writeStartElement("item");

        for (Map.Entry<String, String> attr : series.getKeys().entrySet()) {
          writer.writeStartElement(attr.getKey());
          writer.writeCharacters(attr.getValue());
          writer.writeEndElement();
        }

        writer.writeStartElement("OBS_DIMENSION");
        writer.writeCharacters(obs.getDimension());
        writer.writeEndElement();

        writer.writeStartElement("OBS_VALUE");
        writer.writeCharacters(obs.getValue().toString());
        writer.writeEndElement();

        writer.writeEndElement();
      }
    }

    writer.writeEndElement();
    writer.writeEndDocument();

    writer.flush();
    writer.close();
    fw.close();

    return new File(dest);
  }
}
