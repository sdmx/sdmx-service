package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;
import org.soenda.service.support.export.ExportFactory;
import org.soenda.service.support.export.product.CSVProduct;

public class CsvAdaptee implements ExportAdaptee {

  private char delimiter = ',';
  private boolean includeHeader = true;

  public CsvAdaptee() {}

  public CsvAdaptee(char delimiter) {
    this.delimiter = delimiter;
  }

  public CsvAdaptee(char delimiter, boolean includeHeader) {
    this.delimiter = delimiter;
    this.includeHeader = includeHeader;
  }

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    CSVProduct csvProduct = (CSVProduct) ExportFactory.create("csv", dest);
    csvProduct.setDelimiter(delimiter);
    csvProduct.includeHeader(includeHeader);
    List<String> headers = new ArrayList<>();

    System.out.println(dataset);
    if (dataset.size() > 0) {
      for (DataSeries series : dataset) {
        if (headers.size() == 0) {
          headers.addAll(series.getKeys().keySet());
          headers.add("OBS_DIMENSION");
          headers.add("OBS_VALUE");
          csvProduct.addItem(headers);
        }

        for (Observation obs : series.getObservations()) {
          List<String> values = new ArrayList<>(series.getKeys().values());
          values.add(obs.getDimension());
          values.add(obs.getValue().toString());
          csvProduct.addItem(values);
        }
      }
    } else {
      List<String> empty = new ArrayList<>();
      empty.add("Empty");
      csvProduct.addItem(empty);
    }

    csvProduct.close();

    return new File(dest);
  }
}
