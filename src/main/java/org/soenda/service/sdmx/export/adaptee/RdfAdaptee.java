package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.*;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.VCARD;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.Observation;
import org.soenda.service.support.export.ExportFactory;
import org.soenda.service.support.export.product.RDFProduct;

public class RdfAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    RDFProduct rdfProduct = (RDFProduct) ExportFactory.create("rdf", dest);

    for (DataSeries series : dataset) {
      Set<Map.Entry<String, String>> keys = series.getKeys().entrySet();

      for (Observation obs : series.getObservations()) {
        Resource resource = rdfProduct.getModel().createResource();

        for (Map.Entry<String, String> field : keys) {
          Property prop;

          switch (field.getKey()) {
            case "GEO":
              prop = VCARD.GEO;
              break;
            default:
              prop = VCARD.SOURCE;
              break;
          }

          resource.addProperty(prop, field.getValue());
        }

        resource.addProperty(VCARD.SOURCE, obs.getDimension());
        resource.addProperty(VCARD.SOURCE, obs.getValue().toString());
      }
    }

    rdfProduct.close();

    return new File(dest);
  }
}
