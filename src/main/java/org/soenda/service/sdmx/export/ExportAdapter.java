package org.soenda.service.sdmx.export;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.export.adaptee.*;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.support.util.IOUtils;
import org.soenda.service.support.util.StringUtils;

public class ExportAdapter {

  private Map<String, Object> params = new HashMap<>();

  public ExportAdapter() {}

  public ExportAdapter(Map<String, Object> params) {
    this.params = params;
  }

  public File export(String type, List<DataSeries> dataset) throws Exception {
    switch (type) {
      case "xml":
        return new XmlAdaptee().export(dataset, getFilepath(type, type));

      case "pdf":
        return new PdfAdaptee().export(dataset, getFilepath(type, type));

      case "rdf":
        return new RdfAdaptee().export(dataset, getFilepath(type, type));

      case "json":
        return new JsonAdaptee().export(dataset, getFilepath(type, type));

        // fix: Crash Solution (Using JSOM)
      case "dspl":
        return new JsonAdaptee().export(dataset, getFilepath(type, "zip"));

        // fix: Crash Solution (Using Generic Data)
      case "sdmx-dsd":
        return new SdmxAdaptee(params).export(dataset, getFilepath(type, "xml"));

      case "sdmx-ml":
        return new SdmxAdaptee(params).export(dataset, getFilepath(type, "xml"));

      case "sdmx-edi":
        return new SdmxEdiAdaptee().export(dataset, getFilepath(type, "ges"));

      case "csv":
        char delimiter = params.getOrDefault("delimiter", ",").toString().charAt(0);
        return new CsvAdaptee(delimiter).export(dataset, getFilepath(type, type));

      case "xls":
      case "xlsx":
        return new ExcelAdaptee().export(dataset, getFilepath(type, type));

      default:
        throw new Exception("'" + type + "' type is not supported.");
    }
  }

  /** Export DSD from every data source */
  // public File exportStructure(DataStructure dataStructure) throws Exception {
  //     #DEV
  // }

  private String getFilepath(String type, String ext) {
    return IOUtils.storagePath("tmp/" + type + "-" + StringUtils.uuid()) + "." + ext;
  }

  public void add(String key, Object value) {
    params.put(key, value);
  }
}
