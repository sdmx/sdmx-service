package org.soenda.service.sdmx.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.util.Strings;
import org.soenda.service.support.util.OSCheck;

public class Converter {
  private final String DIR = "lib/converter";

  public static final String FORMAT_CSV = "CSV";
  public static final String FORMAT_MULTI_LEVEL_CSV = "MULTI_LEVEL_CSV";
  public static final String FORMAT_SDMX_CSV = "SDMX_CSV";
  public static final String FORMAT_GESMES_TS = "GESMES_TS";
  public static final String FORMAT_GENERIC_SDMX = "GENERIC_SDMX";
  public static final String FORMAT_COMPACT_SDMX = "COMPACT_SDMX";
  public static final String FORMAT_UTILITY_SDMX = "UTILITY_SDMX";
  public static final String FORMAT_CROSS_SDMX = "CROSS_SDMX";
  public static final String FORMAT_GENERIC_DATA_2_1 = "GENERIC_DATA_2_1";
  public static final String FORMAT_GENERIC_TS_DATA_2_1 = "GENERIC_TS_DATA_2_1";
  public static final String FORMAT_STRUCTURE_SPECIFIC_DATA_2_1 = "STRUCTURE_SPECIFIC_DATA_2_1";
  public static final String FORMAT_STRUCTURE_SPECIFIC_TS_DATA_2_1 =
      "STRUCTURE_SPECIFIC_TS_DATA_2_1";
  public static final String FORMAT_EXCEL = "EXCEL";
  public static final String FORMAT_MESSAGE_GROUP = "MESSAGE_GROUP";

  private List<String> commands = new ArrayList<>();

  public Converter() {
    switch (OSCheck.getOperatingSystemType()) {
      case Windows:
        commands.add("cmd");
        commands.add("/c");
        commands.add("converter-cli.bat");
        break;

      case MacOS:
      case Linux:
      case Other:
        commands.add("./converter-cli.sh");
        break;
    }
  }

  public Converter from(String from) {
    commands.add("-from");
    commands.add(from);
    return this;
  }

  public Converter to(String to) {
    commands.add("-to");
    commands.add(to);
    return this;
  }

  public Converter inputFile(String file) {
    commands.add("-inputFile");
    commands.add("../../" + file);
    return this;
  }

  public Converter outputFile(String file) {
    commands.add("-outputFile");
    commands.add("../../" + file);
    return this;
  }

  public Converter dsdFile(String file) {
    commands.add("-dsd_file");
    commands.add("../../" + file);
    return this;
  }

  public Converter headerFile(String file) {
    commands.add("-header_file");
    commands.add("../../" + file);
    commands.add("-header_row");
    commands.add("true");
    return this;
  }

  public Converter delimiter(String delimiter) {
    commands.add("-delimiter");
    commands.add(delimiter);
    return this;
  }

  public boolean convert() {
    try {
      System.out.println("# " + Strings.join(commands, ' '));
      ProcessBuilder pb = new ProcessBuilder(commands);
      pb.directory(new File(DIR));
      final Process p = pb.start();

      BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line;

      while ((line = br.readLine()) != null) {
        System.out.println(line);
      }

      return true;
    } catch (Exception ex) {
      ex.printStackTrace();
      return false;
    }
  }
}
