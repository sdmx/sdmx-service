package org.soenda.service.sdmx.export.adaptee;

import java.io.File;
import java.util.*;
import org.soenda.service.sdmx.object.DataSeries;

public class DsplAdaptee implements ExportAdaptee {

  @Override
  public File export(List<DataSeries> dataset, String dest) throws Exception {
    //        String dsplPath = IOUtils.storagePath("tmp/"+ StringUtils.uuid());
    //        String dsplFile = dsplPath +"/dspl.xml";
    //        File dsplDir = new File(dsplPath);
    //
    //        if (!dsplDir.mkdirs()) {
    //            throw new Exception("Failed to make directory path to '"+ dsplPath +"'");
    //        }
    //
    //        List<String> ns = Collections.singletonList("time");
    //        Dataflow dataflow = sdmxData.getDataflow(id, "latest").one();
    //        DataStructure dsd = sdmxData.getDataStructure(id).get();
    //        Map<String, CodeList> codelist = dsd.getCodeLists();
    //
    //        FileWriter fw = new FileWriter(dsplFile);
    //        XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fw);
    //
    //        writer.writeStartDocument();
    //        writer.writeStartElement("dspl");
    //        writer.writeAttribute("xmlns", "http://schemas.google.com/dspl/2010");
    //
    //        // Namspaces
    //        for (String namespace : ns) {
    //            writer.writeAttribute("xmlns:"+namespace,
    // "http://www.google.com/publicdata/dataset/google/" + namespace);
    //        }
    //
    //        for (String namespace : ns) {
    //            writer.writeStartElement("import");
    //            writer.writeAttribute("namespace",
    // "http://www.google.com/publicdata/dataset/google/" + namespace);
    //            writer.writeEndElement();
    //        }
    //
    //        // Info
    //        writer.writeStartElement("info");
    //        writer.writeStartElement("name");
    //        writer.writeStartElement("value");
    //        writer.writeCharacters(dataflow.getName());
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //
    //        // Provider
    //        writer.writeStartElement("provider");
    //        writer.writeStartElement("name");
    //        writer.writeStartElement("value");
    //        writer.writeCharacters("Eurostat");
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //
    //        writer.writeStartElement("url");
    //        writer.writeStartElement("value");
    //        writer.writeCharacters("ec.europa.eu");
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //
    //        // Concepts
    //        writer.writeStartElement("concepts");
    //
    //        for (Map.Entry<String, Concept> item : dsd.getConcepts().entrySet()) {
    //            writer.writeStartElement("concept");
    //            writer.writeAttribute("id", item.getKey());
    //            Concept concept = item.getValue();
    //
    //            writer.writeStartElement("info");
    //            writer.writeStartElement("name");
    //            writer.writeStartElement("value");
    //            writer.writeCharacters(concept.getDescription());
    //            writer.writeEndElement();
    //            writer.writeEndElement();
    //
    //            writer.writeStartElement("description");
    //            writer.writeStartElement("value");
    //            writer.writeCharacters(concept.getDescription());
    //            writer.writeEndElement();
    //            writer.writeEndElement();
    //            writer.writeEndElement();
    //
    //            writer.writeStartElement("type");
    //            writer.writeAttribute("ref", getTypeFromName(item.getKey()));
    //            writer.writeEndElement();
    //
    //            String tableRefName = "CL_" + item.getKey();
    //
    //            if (codelist.containsKey(tableRefName)) {
    //                writer.writeStartElement("table");
    //                writer.writeAttribute("ref", tableRefName);
    //                writer.writeEndElement();
    //            }
    //
    //            writer.writeEndElement();
    //        }
    //
    //        writer.writeEndElement();
    //
    //        // Slices
    //        writer.writeStartElement("slices");
    //        writer.writeStartElement("slice");
    //        writer.writeAttribute("id", "data_slice");
    //
    //        for (Map.Entry<String, Dimension> item : dsd.getDimensionList().entrySet()) {
    //            writer.writeStartElement("dimension");
    //            writer.writeAttribute("concept", item.getKey());
    //            writer.writeEndElement();
    //        }
    //
    //        writer.writeStartElement("metric");
    //        writer.writeAttribute("concept", "OBS_VALUE");
    //        writer.writeEndElement();
    //
    //        writer.writeStartElement("table");
    //        writer.writeAttribute("ref", "data_slice_table");
    //        writer.writeEndElement();
    //
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //
    //        // Tables
    //        writer.writeStartElement("tables");
    //
    //        // CodeList table
    //        for (Map.Entry<String, CodeList> item : codelist.entrySet()) {
    //            writer.writeStartElement("table");
    //            writer.writeAttribute("id", item.getKey());
    //
    //            List<String> column = Lists.newArrayList(new String[]{item.getKey().substring(3),
    // "value"});
    //
    //            for (String columnName : column) {
    //                writer.writeStartElement("column");
    //                writer.writeAttribute("id", columnName);
    //                writer.writeAttribute("type", getTypeFromName(columnName));
    //                writer.writeEndElement();
    //            }
    //
    //            // Export Code List to CSV File
    //            String tableFilename = item.getKey() + ".csv";
    //            CSVProduct csvProduct = (CSVProduct) ExportFactory.create("csv", dsplPath +"/"+
    // tableFilename);
    //            csvProduct.includeHeader(true);
    //            csvProduct.setHeaders(column);
    //
    //            Map<String, String> codes = item.getValue().getCodes();
    //
    //            for (Map.Entry<String, String> code : codes.entrySet()) {
    //                csvProduct.addItem(Arrays.asList(new String[]{code.getKey(),
    // code.getValue()}));
    //            }
    //
    //            csvProduct.close();
    //
    //            writer.writeStartElement("data");
    //            writer.writeStartElement("file");
    //            writer.writeAttribute("format", "csv");
    //            writer.writeAttribute("encoding", "utf-8");
    //            writer.writeCharacters(tableFilename);
    //            writer.writeEndElement();
    //            writer.writeEndElement();
    //
    //            writer.writeEndElement();
    //        }
    //
    //        // table data slice
    //        writer.writeStartElement("table");
    //        writer.writeAttribute("id", "data_slice_table");
    //
    //        // Export Dataset to CSV File
    //        String tableFilename = "data_slice.csv";
    ////				Map<String, String> codes = codelist;
    //
    //        CSVProduct csvProduct = (CSVProduct) ExportFactory.create("csv", dsplPath +"/"+
    // tableFilename);
    //        csvProduct.includeHeader(true);
    //        csvProduct.addList(dataset);
    //
    //        for (String header : csvProduct.getHeaders()) {
    //            writer.writeStartElement("column");
    //            writer.writeAttribute("id", header);
    //            writer.writeAttribute("type", getTypeFromName(header));
    //            writer.writeEndElement();
    //        }
    //
    //        csvProduct.close();
    //
    //        writer.writeStartElement("data");
    //        writer.writeStartElement("file");
    //        writer.writeAttribute("format", "csv");
    //        writer.writeAttribute("encoding", "utf-8");
    //        writer.writeCharacters(tableFilename);
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //        writer.writeEndElement();
    //
    //        writer.writeEndElement();
    //        writer.writeEndDocument();
    //
    //        writer.flush();
    //        writer.close();
    //        fw.close();
    //
    //        String filepath = dsplPath + ".zip";
    //        ZipUtil.pack(dsplDir, new File(filepath));

    return new File(dest);
  }

  private String getTypeFromName(String conceptName) {
    switch (conceptName) {
      case "OBS_VALUE":
        return "float";
      default:
        return "string";
    }
  }
}
