package org.soenda.service.controllers.v1;

import com.google.common.base.Joiner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.object.CodeList;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dimension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/data/query")
public class QueryDataController {

  @Autowired private DataSourceAdapter dataSourceAdapter;

  private DataStructure mergeDsd(String[] dataIds) throws Exception {
    DataStructure dsd = new DataStructure();
    int countDim = 0;

    for (String dataId : dataIds) {
      String[] dataIdPart = dataId.split("/");
      DataStructure eachDsd = dataSourceAdapter.getDsd(dataIdPart[0], dataIdPart[1]);

      // Dimensions
      for (Map.Entry<String, Dimension> item : eachDsd.getDimensionList().entrySet()) {
        String dimId = item.getKey();
        Dimension dim = item.getValue();

        // add new dimension
        if (!dsd.hasDimension(dimId)) {
          dim.setPosition(countDim + dim.getPosition());
          dsd.addDimension(dimId, dim);
        }
        // merge codelist
        else {
          Dimension currDim = dsd.getDimension(dimId);
          CodeList currCL = currDim.getCodeList();
          CodeList eachCL = dim.getCodeList();

          for (Map.Entry<String, String> code : eachCL.getCodes().entrySet()) {
            currCL.addCode(code.getKey(), code.getValue());
          }

          currDim.setCodeList(currCL);
          dsd.addDimension(dimId, currDim);
        }
      }

      countDim = dsd.getDimensionList().size();
    }

    return dsd;
  }

  @PostMapping("dimensions")
  public List<Dimension> structureQuery(@RequestParam(value = "dataIds") String[] dataIds)
      throws Exception {
    DataStructure dsd = mergeDsd(dataIds);
    return dsd.getSortedDimensionList();
  }

  @PostMapping("series")
  public List<DataSeries> datasetQuery(
      @RequestParam(value = "startPeriod", required = false, defaultValue = "") String startPeriod,
      @RequestParam(value = "endPeriod", required = false, defaultValue = "") String endPeriod,
      @RequestParam(value = "dataIds") String[] dataIds,
      @RequestParam Map<String, Object> params)
      throws Exception {
    Map<String, Object> queryParam = new HashMap<>();
    queryParam.put("startPeriod", startPeriod);
    queryParam.put("endPeriod", endPeriod);

    // merge dataset
    List<DataSeries> dataset = new ArrayList<>();

    for (String dataId : dataIds) {
      String[] dataIdPart = dataId.split("/");

      DataStructure eachDsd = dataSourceAdapter.getDsd(dataIdPart[0], dataIdPart[1]);
      List<String> filterKeys = new ArrayList<>();

      for (Dimension dim : eachDsd.getSortedDimensionList()) {
        filterKeys.add(params.getOrDefault("filter[" + dim.getId() + "]", "").toString());
      }

      queryParam.put("key", Joiner.on('.').join(filterKeys));
      List<DataSeries> eachDataset =
          dataSourceAdapter.getDataset(dataIdPart[0], dataIdPart[1], queryParam);
      dataset.addAll(eachDataset);
    }

    return dataset;
  }

  /*@PostMapping("{id:.+}/export")
      public ResponseEntity export(
              @PathVariable("provider") String provider,
              @PathVariable("id") String id,
              @RequestParam Map<String, Object> params
      ) throws Exception {
          // Export into 't' file type
          String type = (String) params.getOrDefault("t", "");
  //        FameAdaptee adaptee = (FameAdaptee) dataSourceAdapter.getAdaptee("fame");
  //        File dsdFile = adaptee.getDsdFile(id);

          File file;

          switch (type) {
  //            case "sdmx-dsd":
  //                file = dsdFile;
  //                break;

              default:
  //                params.put("dsdFile", dsdFile.toPath().toString());
                  params.put("id", id);

                  List<DataSeries> dataset = dataSourceAdapter.getDataset(provider, id, params);
                  file = new ExportAdapter(params).export(type, dataset);
                  break;
          }

          // download response
          Path exportedFilepath = file.toPath();
          String exportedFilename = exportedFilepath.getFileName().toString();
          String extension = exportedFilename.substring(exportedFilename.lastIndexOf('.'));
          String filename = String.format("%s-%s.%s", type, id, extension);

          return HttpUtils.downloadResponse(exportedFilepath, filename, true);
      }*/
}
