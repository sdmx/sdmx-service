package org.soenda.service.controllers.v1;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import org.apache.poi.ss.usermodel.*;
import org.soenda.service.support.export.ExportFactory;
import org.soenda.service.support.export.product.ExcelProduct;
import org.soenda.service.support.util.HttpUtils;
import org.soenda.service.support.util.IOUtils;
import org.soenda.service.support.util.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/export")
public class ExportController {

  @PostMapping("chart/{type}")
  public ResponseEntity<byte[]> chartExport(
      @PathVariable("type") String type, @RequestParam("data") String data) {
    byte[] imageData = Base64.getDecoder().decode(data.substring(data.indexOf(',') + 1));
    String filepath = IOUtils.storagePath("tmp/" + StringUtils.uuid()) + "." + type;
    Path path = Paths.get(filepath);

    try {
      switch (type) {
        case "pdf":
          Document document = new Document(PageSize.A4.rotate());
          PdfWriter.getInstance(document, new FileOutputStream(filepath));
          document.open();

          Image img = Image.getInstance(imageData);
          img.scalePercent(
              ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin())
                      / img.getWidth())
                  * 100);

          document.add(img);
          document.close();
          break;

        case "xls":
          ExcelProduct excelProduct = (ExcelProduct) ExportFactory.create(type, filepath);
          excelProduct.createSheet("chart", null);

          Workbook wb = excelProduct.getWorkbook();
          int pictureIdx = wb.addPicture(imageData, Workbook.PICTURE_TYPE_PNG);
          CreationHelper helper = wb.getCreationHelper();
          Drawing drawing = excelProduct.getSheet().createDrawingPatriarch();

          ClientAnchor anchor = helper.createClientAnchor();
          anchor.setCol1(2);
          anchor.setRow1(1);

          Picture picture = drawing.createPicture(anchor, pictureIdx);
          picture.resize();

          drawing.createPicture(anchor, pictureIdx);
          excelProduct.close();
          break;

        case "png":
          IOUtils.filePut(filepath, imageData);
          break;

        default:
          throw new Exception("not found");
      }
    } catch (Exception e) {
      e.printStackTrace();
      //            throw new Exception("Failed to export '"+ type +"' file.");
    }

    return HttpUtils.downloadResponse(path, "chart." + type, true);
  }
}
