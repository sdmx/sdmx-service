package org.soenda.service.controllers.v1;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.datasource.adaptee.FameAdaptee;
import org.soenda.service.sdmx.export.ExportAdapter;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.soenda.service.sdmx.object.Dimension;
import org.soenda.service.sdmx.parser.SeriesParser;
import org.soenda.service.support.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/data/fame")
public class FameDataController {

  @Autowired private Pageable pageable;

  @Autowired private DataSourceAdapter dataSourceAdapter;

  private final String source = "fame:BI";

  @GetMapping
  public Page<Dataflow> list(@RequestParam Map<String, Object> param) throws Exception {
    String query = param.getOrDefault("q", "").toString();
    String category = param.getOrDefault("category", "").toString();
    String searchQuery;

    if (category.length() > 0) {
      searchQuery = category + "*OBS_VALUE";
      boolean isSearchContainsQuery =
          Pattern.compile(Pattern.quote(query), Pattern.CASE_INSENSITIVE)
              .matcher(searchQuery)
              .find();

      if (!isSearchContainsQuery) {
        searchQuery = category + "*" + query + "*OBS_VALUE";
      }
    } else {
      searchQuery = "*" + query + "*OBS_VALUE";
    }

    return dataSourceAdapter.getDataflow(source, searchQuery, pageable);
  }

  @PostMapping
  public Page<Dataflow> filter(@RequestParam Map<String, Object> param) throws Exception {
    String query = param.getOrDefault("query", "").toString();

    return dataSourceAdapter.getDataflow(source, query, pageable);
  }

  @GetMapping("{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("id") String id,
      @RequestParam(value = "key", required = false, defaultValue = "") String filterKey,
      @RequestParam(value = "startPeriod", required = false, defaultValue = "") String startPeriod,
      @RequestParam(value = "endPeriod", required = false, defaultValue = "") String endPeriod)
      throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("obs", id);
    params.put("startPeriod", startPeriod);
    params.put("endPeriod", endPeriod);

    Map<String, Object> res = dataset(id, params);
    res.put("keySeries", filterKey.length() == 0 ? id.substring(id.indexOf('.') + 1) : filterKey);

    return res;
  }

  @PostMapping("{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("id") String id, @RequestParam Map<String, Object> params) throws Exception {
    Map<String, Object> res = new HashMap<>();
    res.put("keySeries", id);
    res.put("startPeriod", params.getOrDefault("startPeriod", "").toString());
    res.put("endPeriod", params.getOrDefault("endPeriod", "").toString());

    // Dimensions
    List<Dimension> dimensions = getDsd(id);
    res.put("dimensions", dimensions);

    // Series Name
    res.put("title", id);

    // Data Series
    List<DataSeries> dataSeries = dataSourceAdapter.getDataset(source, id, params);
    res.put("series", SeriesParser.getInstance().parse(dataSeries));

    return res;
  }

  @GetMapping("dsd/{id:.+}")
  public List<Dimension> getDsd(@PathVariable("id") String id) throws Exception {
    DataStructure dsd = dataSourceAdapter.getDsd(source, id);

    if (dsd == null) {
      return new ArrayList<>();
    }

    return dsd.getSortedDimensionList();
  }

  @PostMapping("{id:.+}/export")
  public ResponseEntity export(
      @PathVariable("id") String id, @RequestParam Map<String, Object> params) throws Exception {
    // Export into 't' file
    String type = (String) params.getOrDefault("t", "");
    FameAdaptee adaptee = (FameAdaptee) dataSourceAdapter.getAdaptee(source);
    File dsdFile = adaptee.getDsdFile(id);

    File file;

    switch (type) {
      case "sdmx-dsd":
        file = dsdFile;
        break;

      default:
        params.put("dsdFile", dsdFile.toPath().toString());
        params.put("id", id);

        List<DataSeries> dataset = dataSourceAdapter.getDataset(source, id, params);
        file = new ExportAdapter(params).export(type, dataset);
        break;
    }

    // download response
    Path exportedFilepath = file.toPath();
    String exportedFilename = exportedFilepath.getFileName().toString();
    String extension = exportedFilename.substring(exportedFilename.lastIndexOf('.'));
    String filename = String.format("%s-%s.%s", type, id, extension);

    return HttpUtils.downloadResponse(exportedFilepath, filename, true);
  }
}
