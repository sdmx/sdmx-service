package org.soenda.service.controllers.v1;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.VCARD;
import org.soenda.service.sdmx.datasource.SdmxData;
import org.soenda.service.sdmx.export.Converter;
import org.soenda.service.sdmx.object.*;
import org.soenda.service.sdmx.parser.DataStructureParser;
import org.soenda.service.sdmx.parser.DatasetParser;
import org.soenda.service.sdmx.parser.SdmxParserException;
import org.soenda.service.support.export.ExportFactory;
import org.soenda.service.support.export.product.CSVProduct;
import org.soenda.service.support.export.product.ExcelProduct;
import org.soenda.service.support.export.product.RDFProduct;
import org.soenda.service.support.util.CollectionUtils;
import org.soenda.service.support.util.IOUtils;
import org.soenda.service.support.util.StringUtils;
import org.zeroturnaround.zip.ZipUtil;

class DataServiceExport {

  private SdmxData sdmxData;

  //  private SdmxBeanRetrievalManager retrievalManager;

  public DataServiceExport(SdmxData sdmxData) {
    this.sdmxData = sdmxData;
  }

  //  @Required
  //  public void setRetrievalManager(SdmxBeanRetrievalManager retrievalManager) {
  //      this.retrievalManager = retrievalManager;
  //  }

  public DatasetParser getDataset(String id, Map<String, Object> param) throws SdmxParserException {
    String key = param.getOrDefault("key", "all").toString();
    String startPeriod = param.getOrDefault("startPeriod", "").toString();
    String endPeriod = param.getOrDefault("endPeriod", "").toString();
    //      String version = param.getOrDefault("version", "2.1").toString();

    // if (version.equals("2.0")) {
    //          org.sdmxsource.sdmx.api.model.
    // }

    return sdmxData.getDataset(
        id,
        key,
        startPeriod.equals("") ? null : startPeriod,
        endPeriod.equals("") ? null : endPeriod);
  }

  public Path export(String type, String id, Map<String, Object> param) throws SdmxParserException {
    try {
      switch (type) {
        case "pdf":
          return exportPdf(getDataset(id, param).getDataMap());

        case "xml":
          return exportXml(getDataset(id, param).getDataMap());

        case "xls":
        case "xlsx":
          return exportXls(getDataset(id, param).getDataMap(), type);

        case "rdf":
          return exportRdf(getDataset(id, param).getDataMap());

        case "csv":
          return exportCsv(
              getDataset(id, param).getDataMap(),
              param.getOrDefault("delimiter", ",").toString().charAt(0));

        case "json":
          return exportJson(getDataset(id, param).getDataMap());

        case "dspl":
          return exportDspl(id, getDataset(id, param).getDataMap());

        case "sdmx-ml":
          String version = param.getOrDefault("version", "2.1").toString();

          if (version.equals("2.0")) {
            return exportSdmxMl20(getDataset(id, param), sdmxData.getDataStructure(id));
          } else {
            return exportSdmxMl21(getDataset(id, param));
          }

        case "sdmx-edi":
          return exportSdmxEDI(getDataset(id, param), sdmxData.getDataStructure(id));

        case "sdmx-dsd":
          return exportSdmxDsd(sdmxData.getDataStructure(id));

        default:
          throw new Exception("'" + type + "' type not supported.");
      }
    } catch (SdmxParserException e) {
      e.printStackTrace();
      throw e;
    } catch (Exception e) {
      e.printStackTrace();
      //          throw new Exception("Failed to export '"+ type +"' file.");
    }

    return null;
  }

  private String generateFilepath(String type, String ext) {
    return IOUtils.storagePath("tmp/" + type + "-" + StringUtils.uuid()) + "." + ext;
  }

  public Path exportSdmxMl21(DatasetParser datasetParser) throws Exception {
    String filepath = generateFilepath("sdmx-ml", "xml");
    File file = new File(filepath);

    Reader reader = datasetParser.getSource().getCharacterStream();
    IOUtils.write(
        file, new ByteArrayInputStream(CharStreams.toString(reader).getBytes(Charsets.UTF_8)));
    reader.close();

    return file.toPath();
  }

  public Path exportSdmxMl20(DatasetParser datasetParser, DataStructureParser dataStructureParser)
      throws Exception {
    String filepath = generateFilepath("sdmx-ml20", "xml");

    new Converter()
        .from(Converter.FORMAT_GENERIC_DATA_2_1)
        .to(Converter.FORMAT_GENERIC_SDMX)
        .inputFile(exportSdmxMl21(datasetParser).toString())
        .outputFile(filepath)
        .dsdFile(exportSdmxDsd(dataStructureParser).toString())
        .convert();

    return new File(filepath).toPath();
  }

  public Path exportSdmxEDI(DatasetParser datasetParser, DataStructureParser dataStructureParser)
      throws Exception {
    String filepath = generateFilepath("sdmx-edi", "ges");
    String inputFile = exportSdmxMl21(datasetParser).toString();
    String dsdFile = exportSdmxDsd(dataStructureParser).toString();

    new Converter()
        .from(Converter.FORMAT_GENERIC_DATA_2_1)
        .to(Converter.FORMAT_GESMES_TS)
        .inputFile(inputFile)
        .outputFile(filepath)
        .dsdFile(dsdFile)
        .convert();

    return new File(filepath).toPath();
  }

  public Path exportSdmxDsd(DataStructureParser dataStructureParser) throws Exception {
    String filepath = generateFilepath("sdmx-dsd", "xml");
    File file = new File(filepath);

    Reader reader = dataStructureParser.getSource().getCharacterStream();
    IOUtils.write(
        file, new ByteArrayInputStream(CharStreams.toString(reader).getBytes(Charsets.UTF_8)));
    reader.close();

    return file.toPath();
  }

  public Path exportXml(List<Map<String, Object>> dataset) throws Exception {
    String filepath = generateFilepath("xml", "xml");
    FileWriter fw = new FileWriter(filepath);
    XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fw);

    writer.writeStartDocument();
    writer.writeStartElement("data");

    for (Map<String, Object> item : dataset) {
      writer.writeStartElement("series");

      for (Map.Entry<String, Object> attr : item.entrySet()) {
        writer.writeStartElement(attr.getKey());
        writer.writeCharacters(attr.getValue().toString());
        writer.writeEndElement();
      }

      writer.writeEndElement();
    }

    writer.writeEndElement();
    writer.writeEndDocument();

    writer.flush();
    writer.close();
    fw.close();

    return Paths.get(filepath);
  }

  private String getTypeFromName(String conceptName) {
    switch (conceptName) {
      case "OBS_VALUE":
        return "float";
      default:
        return "string";
    }
  }

  public Path exportDspl(String id, List<Map<String, Object>> dataset) throws Exception {
    String dsplPath = IOUtils.storagePath("tmp/" + StringUtils.uuid());
    String dsplFile = dsplPath + "/dspl.xml";

    File dsplDir = new File(dsplPath);
    dsplDir.mkdirs();

    List<String> ns = Arrays.asList(new String[] {"time"});
    Dataflow dataflow = sdmxData.getDataflow(id, "latest").one();
    DataStructure dsd = sdmxData.getDataStructure(id).get();
    Map<String, CodeList> codelist = dsd.getCodeLists();

    FileWriter fw = new FileWriter(dsplFile);
    XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fw);

    writer.writeStartDocument();
    writer.writeStartElement("dspl");
    writer.writeAttribute("xmlns", "http://schemas.google.com/dspl/2010");

    // Namspaces
    for (String namespace : ns) {
      writer.writeAttribute(
          "xmlns:" + namespace, "http://www.google.com/publicdata/dataset/google/" + namespace);
    }

    for (String namespace : ns) {
      writer.writeStartElement("import");
      writer.writeAttribute(
          "namespace", "http://www.google.com/publicdata/dataset/google/" + namespace);
      writer.writeEndElement();
    }

    // Info
    writer.writeStartElement("info");
    writer.writeStartElement("name");
    writer.writeStartElement("value");
    writer.writeCharacters(dataflow.getName());
    writer.writeEndElement();
    writer.writeEndElement();
    writer.writeEndElement();

    // Provider
    writer.writeStartElement("provider");
    writer.writeStartElement("name");
    writer.writeStartElement("value");
    writer.writeCharacters("Eurostat");
    writer.writeEndElement();
    writer.writeEndElement();

    writer.writeStartElement("url");
    writer.writeStartElement("value");
    writer.writeCharacters("ec.europa.eu");
    writer.writeEndElement();
    writer.writeEndElement();
    writer.writeEndElement();

    // Concepts
    writer.writeStartElement("concepts");

    for (Map.Entry<String, Concept> item : dsd.getConcepts().entrySet()) {
      writer.writeStartElement("concept");
      writer.writeAttribute("id", item.getKey());
      Concept concept = item.getValue();

      writer.writeStartElement("info");
      writer.writeStartElement("name");
      writer.writeStartElement("value");
      writer.writeCharacters(concept.getDescription());
      writer.writeEndElement();
      writer.writeEndElement();

      writer.writeStartElement("description");
      writer.writeStartElement("value");
      writer.writeCharacters(concept.getDescription());
      writer.writeEndElement();
      writer.writeEndElement();
      writer.writeEndElement();

      writer.writeStartElement("type");
      writer.writeAttribute("ref", getTypeFromName(item.getKey()));
      writer.writeEndElement();

      String tableRefName = "CL_" + item.getKey();

      if (codelist.containsKey(tableRefName)) {
        writer.writeStartElement("table");
        writer.writeAttribute("ref", tableRefName);
        writer.writeEndElement();
      }

      writer.writeEndElement();
    }

    writer.writeEndElement();

    // Slices
    writer.writeStartElement("slices");
    writer.writeStartElement("slice");
    writer.writeAttribute("id", "data_slice");

    for (Map.Entry<String, Dimension> item : dsd.getDimensionList().entrySet()) {
      writer.writeStartElement("dimension");
      writer.writeAttribute("concept", item.getKey());
      writer.writeEndElement();
    }

    writer.writeStartElement("metric");
    writer.writeAttribute("concept", "OBS_VALUE");
    writer.writeEndElement();

    writer.writeStartElement("table");
    writer.writeAttribute("ref", "data_slice_table");
    writer.writeEndElement();

    writer.writeEndElement();
    writer.writeEndElement();

    // Tables
    writer.writeStartElement("tables");

    // CodeList table
    for (Map.Entry<String, CodeList> item : codelist.entrySet()) {
      writer.writeStartElement("table");
      writer.writeAttribute("id", item.getKey());

      List<String> column = Lists.newArrayList(new String[] {item.getKey().substring(3), "value"});

      for (String columnName : column) {
        writer.writeStartElement("column");
        writer.writeAttribute("id", columnName);
        writer.writeAttribute("type", getTypeFromName(columnName));
        writer.writeEndElement();
      }

      // Export Code List to CSV File
      String tableFilename = item.getKey() + ".csv";
      CSVProduct csvProduct =
          (CSVProduct) ExportFactory.create("csv", dsplPath + "/" + tableFilename);
      csvProduct.includeHeader(true);
      csvProduct.setHeaders(column);

      Map<String, String> codes = item.getValue().getCodes();

      for (Map.Entry<String, String> code : codes.entrySet()) {
        csvProduct.addItem(Arrays.asList(new String[] {code.getKey(), code.getValue()}));
      }

      csvProduct.close();

      writer.writeStartElement("data");
      writer.writeStartElement("file");
      writer.writeAttribute("format", "csv");
      writer.writeAttribute("encoding", "utf-8");
      writer.writeCharacters(tableFilename);
      writer.writeEndElement();
      writer.writeEndElement();

      writer.writeEndElement();
    }

    // table data slice
    writer.writeStartElement("table");
    writer.writeAttribute("id", "data_slice_table");

    // Export Dataset to CSV File
    String tableFilename = "data_slice.csv";
    //              Map<String, String> codes = codelist;

    CSVProduct csvProduct =
        (CSVProduct) ExportFactory.create("csv", dsplPath + "/" + tableFilename);
    csvProduct.includeHeader(true);
    csvProduct.addList(dataset);

    for (String header : csvProduct.getHeaders()) {
      writer.writeStartElement("column");
      writer.writeAttribute("id", header);
      writer.writeAttribute("type", getTypeFromName(header));
      writer.writeEndElement();
    }

    csvProduct.close();

    writer.writeStartElement("data");
    writer.writeStartElement("file");
    writer.writeAttribute("format", "csv");
    writer.writeAttribute("encoding", "utf-8");
    writer.writeCharacters(tableFilename);
    writer.writeEndElement();
    writer.writeEndElement();
    writer.writeEndElement();
    writer.writeEndElement();

    writer.writeEndElement();
    writer.writeEndDocument();

    writer.flush();
    writer.close();
    fw.close();

    String filepath = dsplPath + ".zip";
    ZipUtil.pack(dsplDir, new File(filepath));

    return Paths.get(filepath);
  }

  public Path exportRdf(List<Map<String, Object>> dataset) throws Exception {
    String filepath = generateFilepath("rdf", "rdf");
    RDFProduct rdfProduct = (RDFProduct) ExportFactory.create("rdf", filepath);

    for (Map<String, Object> item : dataset) {
      Resource resource = rdfProduct.getModel().createResource();

      for (Map.Entry<String, Object> field : item.entrySet()) {
        Property prop;

        switch (field.getKey()) {
          case "GEO":
            prop = VCARD.GEO;
            break;
          default:
            prop = VCARD.SOURCE;
            break;
        }

        resource.addProperty(prop, field.getValue().toString());
      }
    }

    rdfProduct.close();

    return Paths.get(filepath);
  }

  public Path exportXls(List<Map<String, Object>> dataset, String type) throws Exception {
    String filepath = generateFilepath(type, type);
    ExcelProduct excelProduct = (ExcelProduct) ExportFactory.create(type, filepath);
    excelProduct.createSheet("data", null);

    int i = 0;

    for (Map<String, Object> item : dataset) {
      if (i == 0) {
        List<String> header = new ArrayList<>();
        header.add("No.");

        for (Map.Entry<String, Object> field : item.entrySet()) {
          header.add(field.getKey());
        }

        excelProduct.addItem(header);
      }

      LinkedList<Object> row = new LinkedList<>(item.values());
      row.addFirst(++i);
      excelProduct.addItem(row);
    }

    excelProduct.close();

    return Paths.get(filepath);
  }

  public Path exportCsv(List<Map<String, Object>> dataset, char delimiter) throws Exception {
    String filepath = generateFilepath("csv", "csv");
    CSVProduct csvProduct = (CSVProduct) ExportFactory.create("csv", filepath);
    csvProduct.setDelimiter(delimiter);
    csvProduct.includeHeader(true);

    if (dataset.size() > 0) {
      csvProduct.addList(dataset);
    } else {
      List<String> empty = new ArrayList<>();
      empty.add("Empty");
      csvProduct.addItem(empty);
    }

    csvProduct.close();

    return Paths.get(filepath);
  }

  public Path exportJson(List<Map<String, Object>> dataset) throws Exception {
    String filepath = generateFilepath("json", "json");

    return IOUtils.filePut(filepath, CollectionUtils.toJson(dataset)).toPath();
  }

  public Path exportPdf(List<Map<String, Object>> dataset) throws Exception {
    Document document = new Document();
    String filepath = generateFilepath("pdf", "pdf");
    PdfWriter.getInstance(document, new FileOutputStream(filepath));
    document.open();

    if (dataset.size() == 0) {
      Chunk chunk = new Chunk("Empty");
      document.add(chunk);
    } else {
      PdfPTable table = null;

      for (Map<String, Object> item : dataset) {
        if (table == null) {
          Set<String> keys = item.keySet();
          table = new PdfPTable(keys.size());

          for (String key : keys) {
            PdfPCell header = new PdfPCell();

            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setPhrase(new Phrase(key));
            header.setBorderWidth(2);

            table.addCell(header);
          }
        }

        for (Map.Entry<String, Object> data : item.entrySet()) {
          table.addCell(data.getValue().toString());
        }
      }

      document.add(table);
    }

    document.close();

    return Paths.get(filepath);
  }
}
