package org.soenda.service.controllers.v1;

import it.bancaditalia.oss.sdmx.client.SdmxClientHandler;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.export.ExportAdapter;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.soenda.service.sdmx.object.Dimension;
import org.soenda.service.sdmx.parser.SeriesParser;
import org.soenda.service.support.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/** @deprecated replaced by SdmxDataController */
@RestController
@RequestMapping("/v1/data/api/{source}")
public class ApiDataController {

  @Autowired private Pageable pageable;

  @Autowired private DataSourceAdapter dataSourceAdapter;

  @GetMapping
  public Page<Dataflow> list(
      @PathVariable("source") final String source, @RequestParam final Map<String, Object> param)
      throws Exception {
    final String query = param.getOrDefault("q", "").toString();
    return dataSourceAdapter.getDataflow(source, query, pageable);
  }

  @GetMapping("dataflow/{id:.+}")
  public Dataflow dataflow(
      @PathVariable("source") final String source, @PathVariable("id") final String id)
      throws Exception {
    final Dataflow dataflow = dataSourceAdapter.getDataflowById(source, id);

    if (dataflow.isEmpty()) {
      return new Dataflow(id, "Data");
    }

    return dataflow;
  }

  @GetMapping("data/{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("source") final String source,
      @PathVariable("id") final String id,
      @RequestParam(value = "key", required = false, defaultValue = "") final String filterKey,
      @RequestParam(value = "startPeriod", required = false, defaultValue = "")
          final String startPeriod,
      @RequestParam(value = "endPeriod", required = false, defaultValue = "")
          final String endPeriod)
      throws Exception {
    final Map<String, Object> params = new HashMap<>();
    params.put("key", filterKey);
    params.put("startPeriod", startPeriod);
    params.put("endPeriod", endPeriod);

    final Map<String, Object> res = dataset(source, id, params);

    return res;
  }

  @PostMapping("data/{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("source") final String source,
      @PathVariable("id") final String id,
      @RequestParam final Map<String, Object> params)
      throws Exception {
    final DataStructure dsd = dataSourceAdapter.getDsd(source, id);
    String filterKey = params.getOrDefault("key", "").toString();

    if (filterKey.length() == 0) {
      filterKey = dsd.getDefaultFilterKey();
      params.put("key", filterKey);
    }

    final Map<String, Object> res = new HashMap<>();
    res.put("keySeries", filterKey);
    res.put("startPeriod", params.getOrDefault("startPeriod", "").toString());
    res.put("endPeriod", params.getOrDefault("endPeriod", "").toString());

    // Series Name
    final String[] provider = source.split(":");

    if (provider.length >= 2) {
      final it.bancaditalia.oss.sdmx.api.Dataflow dataflow =
          SdmxClientHandler.getFlow(provider[1], id);
      final String[] dfName = dataflow.getName().split(";");
      res.put("title", dfName[1].trim());
    } else {
      throw new Exception("Invalid data source format. It should be 'source:provider'.");
    }

    // Dimensions
    final List<Dimension> dimensions = dsd.getSortedDimensionList();
    res.put("dimensions", dimensions);

    // Data Series
    final List<DataSeries> dataSeries = dataSourceAdapter.getDataset(source, id, params);
    res.put("series", SeriesParser.getInstance().parse(dataSeries));

    return res;
  }

  /** todo: Should be Generic (Export Merged Dataset) */
  @PostMapping("data/{id:.+}/export")
  public ResponseEntity export(
      @PathVariable("source") final String source,
      @PathVariable("id") final String id,
      @RequestParam final Map<String, Object> params)
      throws Exception {
    // Export into 't' file type
    final String type = (String) params.getOrDefault("t", "");
    //        FameAdaptee adaptee = (FameAdaptee) dataSourceAdapter.getAdaptee("fame");
    //        File dsdFile = adaptee.getDsdFile(id);

    File file;

    switch (type) {
        //            case "sdmx-dsd":
        //                file = dsdFile;
        //                break;

      default:
        //                params.put("dsdFile", dsdFile.toPath().toString());
        params.put("id", id);

        final List<DataSeries> dataset = dataSourceAdapter.getDataset(source, id, params);
        file = new ExportAdapter(params).export(type, dataset);
        break;
    }

    if (file == null) {
      throw new Exception("Export failed.");
    }

    // download response
    final Path exportedFilepath = file.toPath();
    final String exportedFilename = exportedFilepath.getFileName().toString();
    final String extension = exportedFilename.substring(exportedFilename.lastIndexOf('.'));
    final String filename = String.format("%s-%s.%s", type, id, extension);

    return HttpUtils.downloadResponse(exportedFilepath, filename, true);
  }
}
