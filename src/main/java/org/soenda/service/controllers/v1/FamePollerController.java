package org.soenda.service.controllers.v1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.parser.SeriesParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/poller/fame")
public class FamePollerController {

  @Autowired private DataSourceAdapter dataSourceAdapter;

  private final String source = "fame:BI";

  private final String seriesId = "EXIM_BI_1_1_M.M.010700";

  @GetMapping
  public List<Map<String, Object>> poll() throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("obs", "*OBS_VALUE");
    //        params.put("startPeriod", );
    //        params.put("endPeriod", );

    return poll(params);
  }

  @PostMapping
  public List<Map<String, Object>> poll(@RequestParam Map<String, Object> params) throws Exception {
    List<DataSeries> dataSeries = dataSourceAdapter.getDataset(source, seriesId, params);
    return SeriesParser.getInstance().parse(dataSeries);
  }
}
