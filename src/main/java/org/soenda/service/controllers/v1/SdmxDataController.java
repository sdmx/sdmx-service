package org.soenda.service.controllers.v1;

import it.bancaditalia.oss.sdmx.client.SdmxClientHandler;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.export.ExportAdapter;
import org.soenda.service.sdmx.object.DataSeries;
import org.soenda.service.sdmx.object.DataStructure;
import org.soenda.service.sdmx.object.Dataflow;
import org.soenda.service.sdmx.object.Dimension;
import org.soenda.service.sdmx.parser.SeriesParser;
import org.soenda.service.support.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/data/sdmx/{source}")
public class SdmxDataController {

  @Autowired private Pageable pageable;

  @Autowired private DataSourceAdapter dataSourceAdapter;

  @GetMapping
  public Page<Dataflow> list(
      @PathVariable("source") String source, @RequestParam Map<String, Object> param)
      throws Exception {
    String query = param.getOrDefault("q", "").toString();
    return dataSourceAdapter.getDataflow(source, query, pageable);
  }

  @GetMapping("{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("source") String source,
      @PathVariable("id") String id,
      @RequestParam(value = "key", required = false, defaultValue = "") String filterKey,
      @RequestParam(value = "startPeriod", required = false, defaultValue = "") String startPeriod,
      @RequestParam(value = "endPeriod", required = false, defaultValue = "") String endPeriod)
      throws Exception {
    Map<String, Object> params = new HashMap<>();
    params.put("key", filterKey);
    params.put("startPeriod", startPeriod);
    params.put("endPeriod", endPeriod);

    Map<String, Object> res = dataset(source, id, params);

    return res;
  }

  @PostMapping("{id:.+}")
  public Map<String, Object> dataset(
      @PathVariable("source") String source,
      @PathVariable("id") String id,
      @RequestParam Map<String, Object> params)
      throws Exception {
    DataStructure dsd = dataSourceAdapter.getDsd(source, id);
    String filterKey = params.getOrDefault("key", "").toString();

    if (filterKey.length() == 0) {
      filterKey = dsd.getDefaultFilterKey();
      params.put("key", filterKey);
    }

    Map<String, Object> res = new HashMap<>();
    res.put("keySeries", filterKey);
    res.put("startPeriod", params.getOrDefault("startPeriod", "").toString());
    res.put("endPeriod", params.getOrDefault("endPeriod", "").toString());

    // Series Name
    String[] provider = source.split(":");

    if (provider.length >= 2) {
      it.bancaditalia.oss.sdmx.api.Dataflow dataflow = SdmxClientHandler.getFlow(provider[1], id);
      String[] dfName = dataflow.getName().split(";");
      res.put("title", dfName[1].trim());
    } else {
      throw new Exception("Invalid data source format. It should be 'source:provider'.");
    }

    // Dimensions
    List<Dimension> dimensions = dsd.getSortedDimensionList();
    res.put("dimensions", dimensions);

    // Data Series
    List<DataSeries> dataSeries = dataSourceAdapter.getDataset(source, id, params);
    res.put("series", SeriesParser.getInstance().parse(dataSeries));

    return res;
  }

  /** DEPRECATED: replaced with ApiDataController.export */
  @PostMapping("{id:.+}/export")
  public ResponseEntity export(
      @PathVariable("source") String source,
      @PathVariable("id") String id,
      @RequestParam Map<String, Object> params)
      throws Exception {
    String type = (String) params.getOrDefault("t", "");
    params.put("id", id);

    List<DataSeries> dataset = dataSourceAdapter.getDataset(source, id, params);
    File file = new ExportAdapter(params).export(type, dataset);

    if (file == null) {
      throw new Exception("Export failed.");
    }

    // download response
    Path exportedFilepath = file.toPath();
    String exportedFilename = exportedFilepath.getFileName().toString();
    String extension = exportedFilename.substring(exportedFilename.lastIndexOf('.'));
    String filename = String.format("%s-%s%s", type, id, extension);

    return HttpUtils.downloadResponse(exportedFilepath, filename, true);
  }
}
