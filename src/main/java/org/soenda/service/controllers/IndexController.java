package org.soenda.service.controllers;

import java.util.HashMap;
import java.util.Map;
import org.soenda.service.connector.bps.object.*;
import org.soenda.service.sdmx.datasource.DataSourceAdapter;
import org.soenda.service.sdmx.datasource.adaptee.BpsAdaptee;
import org.soenda.service.support.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

  @Autowired private DataSourceAdapter dataSourceAdapter;

  @RequestMapping("/")
  public String index() {
    return "Rest Endpoint";
  }

  @RequestMapping("/bps")
  public ResponseEntity<String> bps() throws Exception {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);

    // dataSourceAdapter.getDataset("api:bps", "0000.192", null);
    // ResponseData response = adaptee.getDomains("all", null);
    // BpsService service = adaptee.getService();

    // String domain = "0000";
    // Integer page = 1;
    // String varId = "192";
    // String lang = "ind";
    // Call<ResponseData> call = service.getDomains(key, "all", null);
    // Call<ResponseData> call = service.getSubjects(key, domain, null, null, null);
    // Call<ResponseData> call = service.getSubjectCategories(key, domain, page, lang);
    // Call<ResponseData> call = service.getVariables(key, domain, null, null, null, false, page,
    // lang);
    // Call<ResponseData> call = service.getDerivedVariables(key, domain, null, null, false, page,
    // lang);
    // Call<ResponseData> call = service.getVerticalVariables(key, domain, null, page, lang);
    // Call<ResponseData> call = service.getPeriodData(key, domain, null, page, lang);
    // Call<ResponseData> call = service.getDerivedPeriodData(key, domain, null, page, lang);
    // Call<ResponseData> call = service.getUnitData(key, domain, page, lang);
    // Call<ResponseData> call = service.getIndicators(key, domain, null, page, lang);
    // Call<ResponseData> call = service.getStatisticTables(key, domain, null, null, page, lang);
    // Call<DataContent> call = service.getData(key, domain, varId, null, null, null, null, lang);
    // Call<ResponseStatisticTable> call = service.getStatisticTable(key, domain, "1902", lang);
    BpsAdaptee adaptee = (BpsAdaptee) dataSourceAdapter.getAdaptee("api:bps");
    // String content = CollectionUtils.toJson(adaptee.getDataflow("0000", null, 2));
    // String content = CollectionUtils.toJson(adaptee.getDsd("0000.192"));

    Map<String, Object> filter = new HashMap<>();
    filter.put("AREA", "3600");
    String content = CollectionUtils.toJson(adaptee.getDataset("0000.192", filter));

    return new ResponseEntity<String>(content, httpHeaders, HttpStatus.OK);
  }
}
