package org.soenda.service.config;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.ehcache.*;
import org.ehcache.config.builders.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

  private static final String RESOURCES_LOCATION = "/resources/";
  private static final String RESOURCES_HANDLER = RESOURCES_LOCATION + "**";

  private static final int MAX_UPLOAD_SIZE = 10 * 1024 * 1024;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**");
    // registry.addMapping("/api/**")
    //     .allowedOrigins("http://rest.soenda.org")
    //     .allowedMethods("PUT", "DELETE")
    //     .allowedHeaders("header1", "header2", "header3")
    //     .exposedHeaders("header1", "header2")
    //     .allowCredentials(false).maxAge(3600);
  }

  @Bean
  public CommonsMultipartResolver multipartResolver() {
    CommonsMultipartResolver cmr = new CommonsMultipartResolver();
    cmr.setMaxUploadSize(MAX_UPLOAD_SIZE * 2);
    cmr.setMaxUploadSizePerFile(MAX_UPLOAD_SIZE); // bytes

    return cmr;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(RESOURCES_LOCATION);
  }

  // @Override
  // public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
  //     configurer.enable();
  // }

  @Bean
  public CacheManager cacheManager() {
    // XmlConfiguration cacheConfig = new
    // XmlConfiguration(getClass().getResource("/config/ehcache.xml"));
    // CacheManager cacheManager = CacheManagerBuilder.newCacheManager(cacheConfig);
    CacheManager cacheManager =
        CacheManagerBuilder.newCacheManagerBuilder()
            .with(
                CacheManagerBuilder.persistence(System.getProperty("user.home") + "/.cache/soenda"))
            // .withCache("sdmx",
            //     CacheConfigurationBuilder
            //         .newCacheConfigurationBuilder(
            //             String.class,
            //             String.class,
            //             ResourcePoolsBuilder.newResourcePoolsBuilder()
            //                 .heap(10, EntryUnit.ENTRIES)
            //                 .offheap(1, MemoryUnit.MB)
            //                 .disk(20, MemoryUnit.MB, true)
            //         )
            //         .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(20)))
            // )
            .build(false);
    cacheManager.init();

    return cacheManager;
  }

  @Bean
  @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
  public Pageable pageable(HttpServletRequest request) {
    Map<String, String[]> httpQuery = request.getParameterMap();

    /** Pagination */
    int page = Integer.parseInt(httpQuery.getOrDefault("page", new String[] {"1"})[0]);
    int pageSize = Integer.parseInt(httpQuery.getOrDefault("size", new String[] {"20"})[0]);

    // default
    if (page < 1) {
      page = 1;
    }

    page -= 1;

    /** Sorting */
    int i = 0;
    Sort.Order[] orders = {};
    String[] sorts = httpQuery.get("sort");

    if (sorts != null) {
      int sortLen = sorts.length;

      for (String column : httpQuery.get("col")) {
        Sort.Direction direction = Sort.Direction.ASC;

        if (sortLen <= i && sorts[i].equals("desc")) {
          direction = Sort.Direction.DESC;
        }

        orders[i] = new Sort.Order(direction, column);
        i++;
      }

      if (orders.length > 0) {
        return PageRequest.of(page, pageSize, Sort.by(orders));
      }
    }

    return PageRequest.of(page, pageSize);
  }

  //    @Controller
  //    static class FaviconController {
  //        @RequestMapping("favicon.ico")
  //        String favicon() {
  //            return "forward:resources/img/favicon.ico";
  //        }
  //    }
}
