@echo off
rem ---------------------------------------------------------------------------
rem
rem   JAVA_HOME       Must point at your Java Development Kit installation.
rem                   Required to run the with the "debug" argument.
rem
rem ---------------------------------------------------------------------------

if not exist "%JAVA_HOME%" (
    echo JAVA_HOME environment variable does not exist
	echo JAVA_HOME must point to your Java installation in order to lunch Converter application
	pause
	goto done
)

set CLASSPATH=.\converter-cli.jar;.\config\*

"%JAVA_HOME%\bin\java" -Xmx1024m -classpath %Classpath%; com.intrasoft.converterCLI.ConverterCLI %*
